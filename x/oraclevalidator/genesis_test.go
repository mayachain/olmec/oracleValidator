package oraclevalidator_test

import (
	"testing"

	keepertest "github.com/edx04/oracleValidator/testutil/keeper"
	"github.com/edx04/oracleValidator/testutil/nullify"
	"github.com/edx04/oracleValidator/x/oraclevalidator"
	"github.com/edx04/oracleValidator/x/oraclevalidator/types"
	"github.com/stretchr/testify/require"
)

func TestGenesis(t *testing.T) {
	genesisState := types.GenesisState{
		Params: types.DefaultParams(),

		// this line is used by starport scaffolding # genesis/test/state
	}

	k, ctx := keepertest.OraclevalidatorKeeper(t)
	oraclevalidator.InitGenesis(ctx, *k, genesisState)
	got := oraclevalidator.ExportGenesis(ctx, *k)
	require.NotNil(t, got)

	nullify.Fill(&genesisState)
	nullify.Fill(got)

	// this line is used by starport scaffolding # genesis/test/assert
}
