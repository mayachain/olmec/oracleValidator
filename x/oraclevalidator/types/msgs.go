package types

import (
	"github.com/tendermint/tendermint/crypto/tmhash"

	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkerrors "github.com/cosmos/cosmos-sdk/types/errors"
)

// ensure Msg interface compliance at compile time
var (
	_ sdk.Msg = &MsgDelegateFeedConsent{}
	_ sdk.Msg = &MsgAggregateValidatorPrevote{}
	_ sdk.Msg = &MsgAggregateValidatorVote{}
)

// oracle message types
const (
	TypeMsgDelegateFeedConsent       = "delegate_feeder"
	TypeMsgAggregateValidatorPrevote = "aggregate_exchange_rate_prevote"
	TypeMsgAggregateValidatorVote    = "aggregate_exchange_rate_vote"
)

//-------------------------------------------------
//-------------------------------------------------

// NewMsgAggregateValidatorPrevote returns MsgAggregateValidatorPrevote instance
func NewMsgAggregateValidatorPrevote(hash AggregateVoteHash, feeder sdk.AccAddress, validator sdk.ValAddress) *MsgAggregateValidatorPrevote {
	return &MsgAggregateValidatorPrevote{
		Hash:      hash.String(),
		Feeder:    feeder.String(),
		Validator: validator.String(),
	}
}

// Route implements sdk.Msg
func (msg MsgAggregateValidatorPrevote) Route() string { return RouterKey }

// Type implements sdk.Msg
func (msg MsgAggregateValidatorPrevote) Type() string { return TypeMsgAggregateValidatorPrevote }

// GetSignBytes implements sdk.Msg
func (msg MsgAggregateValidatorPrevote) GetSignBytes() []byte {
	return sdk.MustSortJSON(ModuleCdc.MustMarshalJSON(&msg))
}

// GetSigners implements sdk.Msg
func (msg MsgAggregateValidatorPrevote) GetSigners() []sdk.AccAddress {
	feeder, err := sdk.AccAddressFromBech32(msg.Feeder)
	if err != nil {
		panic(err)
	}

	return []sdk.AccAddress{feeder}
}

// ValidateBasic Implements sdk.Msg
func (msg MsgAggregateValidatorPrevote) ValidateBasic() error {

	_, err := AggregateVoteHashFromHexString(msg.Hash)
	if err != nil {
		return sdkerrors.Wrapf(ErrInvalidHash, "Invalid vote hash (%s)", err)
	}

	// HEX encoding doubles the hash length
	if len(msg.Hash) != tmhash.TruncatedSize*2 {
		return ErrInvalidHashLength
	}

	_, err = sdk.AccAddressFromBech32(msg.Feeder)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "Invalid feeder address (%s)", err)
	}

	_, err = sdk.ValAddressFromBech32(msg.Validator)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "Invalid operator address (%s)", err)
	}

	return nil
}

// NewMsgAggregateValidatorVote returns MsgAggregateValidatorVote instance
func NewMsgAggregateValidatorVote(salt string, validatorsAddress string, feeder sdk.AccAddress, validator sdk.ValAddress) *MsgAggregateValidatorVote {
	return &MsgAggregateValidatorVote{
		Salt:              salt,
		ValidatorsAddress: validatorsAddress,
		Feeder:            feeder.String(),
		Validator:         validator.String(),
	}
}

// Route implements sdk.Msg
func (msg MsgAggregateValidatorVote) Route() string { return RouterKey }

// Type implements sdk.Msg
func (msg MsgAggregateValidatorVote) Type() string { return TypeMsgAggregateValidatorVote }

// GetSignBytes implements sdk.Msg
func (msg MsgAggregateValidatorVote) GetSignBytes() []byte {
	return sdk.MustSortJSON(ModuleCdc.MustMarshalJSON(&msg))
}

// GetSigners implements sdk.Msg
func (msg MsgAggregateValidatorVote) GetSigners() []sdk.AccAddress {
	feeder, err := sdk.AccAddressFromBech32(msg.Feeder)
	if err != nil {
		panic(err)
	}

	return []sdk.AccAddress{feeder}
}

// ValidateBasic implements sdk.Msg
func (msg MsgAggregateValidatorVote) ValidateBasic() error {

	_, err := sdk.AccAddressFromBech32(msg.Feeder)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "Invalid feeder address (%s)", err)
	}

	_, err = sdk.ValAddressFromBech32(msg.Validator)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "Invalid operator address (%s)", err)
	}

	if l := len(msg.ValidatorsAddress); l == 0 {
		return sdkerrors.Wrap(sdkerrors.ErrUnknownRequest, "must provide at least one validator address")
	} else if l > 130 {
		return sdkerrors.Wrap(sdkerrors.ErrInvalidRequest, "")
	}

	_, err = ParseValidators(msg.ValidatorsAddress)
	if err != nil {
		return sdkerrors.Wrap(ErrInvalidValidatorList, "failed to parse validators string cause: "+err.Error())
	}

	if len(msg.Salt) > 4 || len(msg.Salt) < 1 {
		return sdkerrors.Wrap(ErrInvalidSaltLength, "salt length must be [1, 4]")
	}

	return nil
}

// NewMsgDelegateFeedConsent creates a MsgDelegateFeedConsent instance
func NewMsgDelegateFeedConsent(operatorAddress sdk.ValAddress, feederAddress sdk.AccAddress) *MsgDelegateFeedConsent {
	return &MsgDelegateFeedConsent{
		Operator: operatorAddress.String(),
		Delegate: feederAddress.String(),
	}
}

// Route implements sdk.Msg
func (msg MsgDelegateFeedConsent) Route() string { return RouterKey }

// Type implements sdk.Msg
func (msg MsgDelegateFeedConsent) Type() string { return TypeMsgDelegateFeedConsent }

// GetSignBytes implements sdk.Msg
func (msg MsgDelegateFeedConsent) GetSignBytes() []byte {
	return sdk.MustSortJSON(ModuleCdc.MustMarshalJSON(&msg))
}

// GetSigners implements sdk.Msg
func (msg MsgDelegateFeedConsent) GetSigners() []sdk.AccAddress {
	operator, err := sdk.ValAddressFromBech32(msg.Operator)
	if err != nil {
		panic(err)
	}

	return []sdk.AccAddress{sdk.AccAddress(operator)}
}

// ValidateBasic implements sdk.Msg
func (msg MsgDelegateFeedConsent) ValidateBasic() error {
	_, err := sdk.ValAddressFromBech32(msg.Operator)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "Invalid operator address (%s)", err)
	}

	_, err = sdk.AccAddressFromBech32(msg.Delegate)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "Invalid delegate address (%s)", err)
	}

	return nil
}
