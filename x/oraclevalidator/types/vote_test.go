package types

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestParseValidators(t *testing.T) {
	valid := "terravalconspub1zcjduepqs5s0vddx5m65h5ntjzwd0x8g3245rgrytpds4ds7vdtlwx06mcesmnkzly"
	_, err := ParseValidators(valid)
	require.NoError(t, err)

	duplicatedAddress := "terravalconspub1zcjduepqs5s0vddx5m65h5ntjzwd0x8g3245rgrytpds4ds7vdtlwx06mcesmnkzly,terravalconspub1zcjduepqs5s0vddx5m65h5ntjzwd0x8g3245rgrytpds4ds7vdtlwx06mcesmnkzly"
	_, err = ParseValidators(duplicatedAddress)
	require.Error(t, err)

	invalidAddress := "terwavalconspub1zcjduepqs5s0vddx5m65h5ntjzwd0x8g3245rgrytpds4ds7vdtlwx06mcesmnkzly"
	_, err = ParseValidators(invalidAddress)
	require.Error(t, err)

	emptyList := ""
	_, err = ParseValidators(emptyList)
	require.Error(t, err)

}
