package types

import (
	sdk "github.com/cosmos/cosmos-sdk/types"
	"github.com/cosmos/cosmos-sdk/types/address"
)

const (
	// ModuleName defines the module name
	ModuleName = "oraclevalidator"

	// StoreKey defines the primary module store key
	StoreKey = ModuleName

	// RouterKey is the message route for slashing
	RouterKey = ModuleName

	// QuerierRoute defines the module's query routing key
	QuerierRoute = ModuleName

	// MemStoreKey defines the in-memory store key
	MemStoreKey = "mem_oraclevalidator"
)

var (
	// Keys for store prefixes
	ValidatorsWhitelistKey       = []byte{0x01} // prefix for each key to a rate
	FeederDelegationKey          = []byte{0x02} // prefix for each key to a feeder delegation
	MissCounterKey               = []byte{0x03} // prefix for each key to a miss counter
	AggregateValidatorPrevoteKey = []byte{0x04} // prefix for each key to a aggregate prevote
	AggregateValidatorVoteKey    = []byte{0x05} // prefix for each key to a aggregate vote
)

func KeyPrefix(p string) []byte {
	return []byte(p)
}

// GetValidatorsWhitelistKey - stored by *denom*
func GetValidatorsWhitelistKey(address string) []byte {
	return append(ValidatorsWhitelistKey, []byte(address)...)
}

// GetFeederDelegationKey - stored by *Validator* address
func GetFeederDelegationKey(v sdk.ValAddress) []byte {
	return append(FeederDelegationKey, address.MustLengthPrefix(v)...)
}

// GetMissCounterKey - stored by *Validator* address
func GetMissCounterKey(v sdk.ValAddress) []byte {
	return append(MissCounterKey, address.MustLengthPrefix(v)...)
}

// GetAggregateValidatorPrevoteKey - stored by *Validator* address
func GetAggregateValidatorPrevoteKey(v sdk.ValAddress) []byte {
	return append(AggregateValidatorPrevoteKey, address.MustLengthPrefix(v)...)
}

// GetAggregateValidatorVoteKey - stored by *Validator* address
func GetAggregateValidatorVoteKey(v sdk.ValAddress) []byte {
	return append(AggregateValidatorVoteKey, address.MustLengthPrefix(v)...)
}
