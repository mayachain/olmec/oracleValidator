package types

import (
	sdk "github.com/cosmos/cosmos-sdk/types"
)

// VoteForTally is a convenience wrapper to reduce redundant lookup cost
type VoteForTally struct {
	Address string
	Voter   sdk.ValAddress
	Power   int64
}

// NewVoteForTally returns a new VoteForTally instance
func NewVoteForTally(address string, voter sdk.ValAddress, power int64) VoteForTally {
	return VoteForTally{
		Address: address,
		Voter:   voter,
		Power:   power,
	}
}

// ExchangeRateBallot is a convenience wrapper around a AddressVote slice
type AddressBallot []VoteForTally

// Claim is an interface that directs its rewards to an attached bank account.
type Claim struct {
	Power     int64
	Weight    int64
	WinCount  int64
	Recipient sdk.ValAddress
}

// NewClaim generates a Claim instance.
func NewClaim(power, weight, winCount int64, recipient sdk.ValAddress) Claim {
	return Claim{
		Power:     power,
		Weight:    weight,
		WinCount:  winCount,
		Recipient: recipient,
	}
}

// ToMap return organized exchange rate map by validator
func (pb AddressBallot) ToMap() map[string]string {
	validatorMap := make(map[string]string)
	for _, vote := range pb {
		validatorMap[string(vote.Voter)] = vote.Address

	}

	return validatorMap
}
