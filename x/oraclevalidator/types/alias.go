package types

import (
	"github.com/edx04/oracleValidator/x/oraclevalidator/types/util"
)

const (
	BlocksPerMinute = util.BlocksPerMinute
	BlocksPerHour   = util.BlocksPerHour
	BlocksPerDay    = util.BlocksPerDay
	BlocksPerWeek   = util.BlocksPerWeek
	BlocksPerMonth  = util.BlocksPerMonth
	BlocksPerYear   = util.BlocksPerYear
)

var (
	IsPeriodLastBlock = util.IsPeriodLastBlock
	AddressVerifier   = util.AddressVerifier
)
