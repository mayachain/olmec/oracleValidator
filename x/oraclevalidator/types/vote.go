package types

import (
	"fmt"
	"strings"

	sdk "github.com/cosmos/cosmos-sdk/types"
	"gopkg.in/yaml.v2"
)

const MAX_NUM_VALIDATORS = 130

type Validators []Validator

// NewAggregateValidatorPrevote returns AggregateValidatorPrevote object
func NewAggregateValidatorPrevote(hash AggregateVoteHash, voter sdk.ValAddress, submitBlock uint64) AggregateValidatorPrevote {
	return AggregateValidatorPrevote{
		Hash:        hash.String(),
		Voter:       voter.String(),
		SubmitBlock: submitBlock,
	}
}

// String implement stringify
func (v AggregateValidatorPrevote) String() string {
	out, _ := yaml.Marshal(v)
	return string(out)
}

// NewAggregateValidatorVote creates a AggregateValidatorVote instance
func NewAggregateValidatorVote(validators Validators, voter sdk.ValAddress) AggregateValidatorVote {
	return AggregateValidatorVote{
		Validators: validators,
		Voter:      voter.String(),
	}
}

// String implement stringify
func (v AggregateValidatorVote) String() string {
	out, _ := yaml.Marshal(v)
	return string(out)
}

func ParseValidators(validatorsStr string) (Validators, error) {
	validatorsStr = strings.TrimSpace(validatorsStr)

	if len(validatorsStr) == 0 {
		return nil, nil
	}

	validatorsStrs := strings.Split(validatorsStr, ",")
	listValidators := make(Validators, len(validatorsStrs))
	duplicateCheckMap := make(map[string]bool)

	if len(validatorsStr) > MAX_NUM_VALIDATORS {
		return nil, ErrExceedNumberOfValidators
	}

	for i, valStr := range validatorsStrs {
		addr, err := sdk.AccAddressFromBech32(valStr)
		if err != nil {
			return nil, err
		}
		addrString := addr.String()
		listValidators[i] = Validator{
			Address: addrString,
		}

		if _, ok := duplicateCheckMap[addrString]; ok {
			return nil, fmt.Errorf("duplicated address %s", addrString)
		}

		duplicateCheckMap[addrString] = true

	}
	return listValidators, nil
}
