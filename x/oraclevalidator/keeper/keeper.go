package keeper

import (
	"fmt"

	"github.com/tendermint/tendermint/libs/log"

	"github.com/cosmos/cosmos-sdk/codec"
	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkerrors "github.com/cosmos/cosmos-sdk/types/errors"
	paramstypes "github.com/cosmos/cosmos-sdk/x/params/types"
	paramtypes "github.com/cosmos/cosmos-sdk/x/params/types"
	stakingtypes "github.com/cosmos/cosmos-sdk/x/staking/types"
	"github.com/edx04/oracleValidator/x/oraclevalidator/types"
)

type (
	Keeper struct {
		cdc        codec.BinaryCodec
		storeKey   sdk.StoreKey
		memKey     sdk.StoreKey
		paramSpace paramtypes.Subspace

		accountKeeper types.AccountKeeper
		bankKeeper    types.BankKeeper
		distrKeeper   types.DistributionKeeper
		StakingKeeper types.StakingKeeper

		distrName string
	}
)

func NewKeeper(
	cdc codec.BinaryCodec, storeKey sdk.StoreKey,
	paramspace paramstypes.Subspace, accountKeeper types.AccountKeeper,
	bankKeeper types.BankKeeper, distrKeeper types.DistributionKeeper,
	stakingKeeper types.StakingKeeper, distrName string,
) *Keeper {
	// set KeyTable if it has not already been set
	if !paramspace.HasKeyTable() {
		paramspace = paramspace.WithKeyTable(types.ParamKeyTable())
	}

	return &Keeper{
		cdc:           cdc,
		storeKey:      storeKey,
		paramSpace:    paramspace,
		accountKeeper: accountKeeper,
		bankKeeper:    bankKeeper,
		distrKeeper:   distrKeeper,
		StakingKeeper: stakingKeeper,
		distrName:     distrName,
	}
}

func (k Keeper) Logger(ctx sdk.Context) log.Logger {
	return ctx.Logger().With("module", fmt.Sprintf("x/%s", types.ModuleName))
}

//-----------------------------------
// ExchangeRate logic

// GetLunaExchangeRate gets the consensus exchange rate of Luna denominated in the denom asset from the store.
func (k Keeper) GetAddressWhitelist(ctx sdk.Context, address string) (bool, error) {

	_, err := sdk.AccAddressFromBech32(address)
	if err != nil {
		return false, err
	}

	store := ctx.KVStore(k.storeKey)
	b := store.Get(types.GetValidatorsWhitelistKey(address))

	if b == nil {
		return false, nil
	}
	return true, nil
}

// SetLunaExchangeRate sets the consensus exchange rate of Luna denominated in the denom asset to the store.
func (k Keeper) SetAddressWhitelist(ctx sdk.Context, address string) {
	store := ctx.KVStore(k.storeKey)
	store.Set(types.GetValidatorsWhitelistKey(address), []byte(address))
}

// SetLunaExchangeRateWithEvent sets the consensus exchange rate of Luna
// denominated in the denom asset to the store with ABCI event
func (k Keeper) SetAddressWhitelistWithEvent(ctx sdk.Context, address string) {
	k.SetAddressWhitelist(ctx, address)
	ctx.EventManager().EmitEvent(
		sdk.NewEvent(types.EventTypeValidatorsWhitelistUpdate,
			sdk.NewAttribute(types.AttributeKeyAddress, address),
		),
	)
}

// DeleteAddress deletes address form whitelist.
func (k Keeper) DeleteAddressFromWhitelist(ctx sdk.Context, address string) {
	store := ctx.KVStore(k.storeKey)
	store.Delete(types.GetValidatorsWhitelistKey(address))
}

// IterateAddress iterates over validator address in the whitelist
func (k Keeper) IterateAddressWhitelist(ctx sdk.Context) (validators []string) {
	store := ctx.KVStore(k.storeKey)
	iter := sdk.KVStorePrefixIterator(store, types.ValidatorsWhitelistKey)
	defer iter.Close()
	for ; iter.Valid(); iter.Next() {
		addr := string(iter.Key()[len(types.ValidatorsWhitelistKey):])
		validators = append(validators, addr)
	}

	return
}

//-----------------------------------
// AggregateValidatorPrevote logic

// GetAggregateValidatorPrevote retrieves an oraclevaldiator prevote from the store
func (k Keeper) GetAggregateValidatorPrevote(ctx sdk.Context, voter sdk.ValAddress) (aggregatePrevote types.AggregateValidatorPrevote, err error) {
	store := ctx.KVStore(k.storeKey)
	b := store.Get(types.GetAggregateValidatorPrevoteKey(voter))
	if b == nil {
		err = sdkerrors.Wrap(types.ErrNoAggregatePrevote, voter.String())
		return
	}
	k.cdc.MustUnmarshal(b, &aggregatePrevote)
	return
}

// SetAggregateValidatorPrevote set an oracle validator aggregate prevote to the store
func (k Keeper) SetAggregateValidatorPrevote(ctx sdk.Context, voter sdk.ValAddress, prevote types.AggregateValidatorPrevote) {
	store := ctx.KVStore(k.storeKey)
	bz := k.cdc.MustMarshal(&prevote)

	store.Set(types.GetAggregateValidatorPrevoteKey(voter), bz)
}

// DeleteAggregateValidatorPrevote deletes an oracle prevote from the store
func (k Keeper) DeleteAggregateValidatorPrevote(ctx sdk.Context, voter sdk.ValAddress) {
	store := ctx.KVStore(k.storeKey)
	store.Delete(types.GetAggregateValidatorPrevoteKey(voter))
}

// IterateAggregateValidatorPrevotes iterates rate over prevotes in the store
func (k Keeper) IterateAggregateValidatorPrevotes(ctx sdk.Context, handler func(voterAddr sdk.ValAddress, aggregatePrevote types.AggregateValidatorPrevote) (stop bool)) {
	store := ctx.KVStore(k.storeKey)
	iter := sdk.KVStorePrefixIterator(store, types.AggregateValidatorPrevoteKey)
	defer iter.Close()
	for ; iter.Valid(); iter.Next() {
		voterAddr := sdk.ValAddress(iter.Key()[2:])

		var aggregatePrevote types.AggregateValidatorPrevote
		k.cdc.MustUnmarshal(iter.Value(), &aggregatePrevote)
		if handler(voterAddr, aggregatePrevote) {
			break
		}
	}
}

//-----------------------------------
// AggregateValidatorVote logic

// GetAggregateValidatorVote retrieves an oraclevalidator prevote from the store
func (k Keeper) GetAggregateValidatorVote(ctx sdk.Context, voter sdk.ValAddress) (aggregateVote types.AggregateValidatorVote, err error) {
	store := ctx.KVStore(k.storeKey)
	b := store.Get(types.GetAggregateValidatorVoteKey(voter))
	if b == nil {
		err = sdkerrors.Wrap(types.ErrNoAggregateVote, voter.String())
		return
	}
	k.cdc.MustUnmarshal(b, &aggregateVote)
	return
}

// SetAggregateValidatorVote adds an oracle aggregate prevote to the store
func (k Keeper) SetAggregateValidatorVote(ctx sdk.Context, voter sdk.ValAddress, vote types.AggregateValidatorVote) {
	store := ctx.KVStore(k.storeKey)
	bz := k.cdc.MustMarshal(&vote)
	store.Set(types.GetAggregateValidatorVoteKey(voter), bz)
}

// DeleteAggregateValdiatorVote deletes an oracle prevote from the store
func (k Keeper) DeleteAggregateValidatorVote(ctx sdk.Context, voter sdk.ValAddress) {
	store := ctx.KVStore(k.storeKey)
	store.Delete(types.GetAggregateValidatorVoteKey(voter))
}

// IterateAggregateValidatorVotes iterates rate over prevotes in the store
func (k Keeper) IterateAggregateValidatorVotes(ctx sdk.Context, handler func(voterAddr sdk.ValAddress, aggregateVote types.AggregateValidatorVote) (stop bool)) {
	store := ctx.KVStore(k.storeKey)
	iter := sdk.KVStorePrefixIterator(store, types.AggregateValidatorVoteKey)
	defer iter.Close()
	for ; iter.Valid(); iter.Next() {
		voterAddr := sdk.ValAddress(iter.Key()[2:])

		var aggregateVote types.AggregateValidatorVote
		k.cdc.MustUnmarshal(iter.Value(), &aggregateVote)
		if handler(voterAddr, aggregateVote) {
			break
		}
	}
}

//-----------------------------------
// Oracle delegation logic

// GetFeederDelegation gets the account address that the validator operator delegated oracle vote rights to
func (k Keeper) GetFeederDelegation(ctx sdk.Context, operator sdk.ValAddress) sdk.AccAddress {
	store := ctx.KVStore(k.storeKey)
	bz := store.Get(types.GetFeederDelegationKey(operator))
	if bz == nil {
		// By default the right is delegated to the validator itself
		return sdk.AccAddress(operator)
	}

	return sdk.AccAddress(bz)
}

// SetFeederDelegation sets the account address that the validator operator delegated oracle vote rights to
func (k Keeper) SetFeederDelegation(ctx sdk.Context, operator sdk.ValAddress, delegatedFeeder sdk.AccAddress) {
	store := ctx.KVStore(k.storeKey)
	store.Set(types.GetFeederDelegationKey(operator), delegatedFeeder.Bytes())
}

// IterateFeederDelegations iterates over the feed delegates and performs a callback function.
func (k Keeper) IterateFeederDelegations(ctx sdk.Context,
	handler func(delegator sdk.ValAddress, delegate sdk.AccAddress) (stop bool)) {

	store := ctx.KVStore(k.storeKey)
	iter := sdk.KVStorePrefixIterator(store, types.FeederDelegationKey)
	defer iter.Close()
	for ; iter.Valid(); iter.Next() {
		delegator := sdk.ValAddress(iter.Key()[2:])
		delegate := sdk.AccAddress(iter.Value())

		if handler(delegator, delegate) {
			break
		}
	}
}

// ValidateFeeder return the given feeder is allowed to feed the message or not
func (k Keeper) ValidateFeeder(ctx sdk.Context, feederAddr sdk.AccAddress, validatorAddr sdk.ValAddress) error {
	if !feederAddr.Equals(validatorAddr) {
		delegate := k.GetFeederDelegation(ctx, validatorAddr)
		if !delegate.Equals(feederAddr) {
			return sdkerrors.Wrap(types.ErrNoVotingPermission, feederAddr.String())
		}
	}

	// Check that the given validator exists
	if val := k.StakingKeeper.Validator(ctx, validatorAddr); val == nil || !val.IsBonded() {
		return sdkerrors.Wrapf(stakingtypes.ErrNoValidatorFound, "validator %s is not active set", validatorAddr.String())
	}

	return nil
}
