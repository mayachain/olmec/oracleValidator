package keeper

import (
	"context"
	"fmt"

	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkerrors "github.com/cosmos/cosmos-sdk/types/errors"
	stakingtypes "github.com/cosmos/cosmos-sdk/x/staking/types"
	"github.com/edx04/oracleValidator/x/oraclevalidator/types"
)

type msgServer struct {
	Keeper
}

// NewMsgServerImpl returns an implementation of the MsgServer interface
// for the provided Keeper.
func NewMsgServerImpl(keeper Keeper) types.MsgServer {
	return &msgServer{Keeper: keeper}
}

var _ types.MsgServer = msgServer{}

func (ms msgServer) AggregateValidatorPrevote(goCtx context.Context, msg *types.MsgAggregateValidatorPrevote) (*types.MsgAggregateValidatorPrevoteResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)

	valAddr, err := sdk.ValAddressFromBech32(msg.Validator)
	if err != nil {
		return nil, err
	}

	feederAddr, err := sdk.AccAddressFromBech32(msg.Feeder)
	if err != nil {
		return nil, err
	}

	if err := ms.ValidateFeeder(ctx, feederAddr, valAddr); err != nil {
		return nil, err
	}

	// Convert hex string to votehash
	voteHash, err := types.AggregateVoteHashFromHexString(msg.Hash)
	if err != nil {
		return nil, sdkerrors.Wrap(types.ErrInvalidHash, err.Error())
	}

	aggregatePrevote := types.NewAggregateValidatorPrevote(voteHash, valAddr, uint64(ctx.BlockHeight()))
	ms.SetAggregateValidatorPrevote(ctx, valAddr, aggregatePrevote)

	ctx.EventManager().EmitEvents(sdk.Events{
		sdk.NewEvent(
			types.EventTypeAggregatePrevote,
			sdk.NewAttribute(types.AttributeKeyVoter, msg.Validator),
		),
		sdk.NewEvent(
			sdk.EventTypeMessage,
			sdk.NewAttribute(sdk.AttributeKeyModule, types.AttributeValueCategory),
			sdk.NewAttribute(sdk.AttributeKeySender, msg.Feeder),
		),
	})

	return &types.MsgAggregateValidatorPrevoteResponse{}, nil
}

func (ms msgServer) AggregateValidatorVote(goCtx context.Context, msg *types.MsgAggregateValidatorVote) (*types.MsgAggregateValidatorVoteResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)
	fmt.Println("heeeeeeeeeeeeeeeeeeeeereeeeeeeeeeeeeeeeeeeeeee")
	valAddr, err := sdk.ValAddressFromBech32(msg.Validator)
	if err != nil {
		fmt.Println("Error 1", err)
		return nil, err
	}

	feederAddr, err := sdk.AccAddressFromBech32(msg.Feeder)
	if err != nil {
		fmt.Println("Error 2", err)
		return nil, err
	}

	if err := ms.ValidateFeeder(ctx, feederAddr, valAddr); err != nil {
		fmt.Println("Error 3", err)
		return nil, err
	}

	params := ms.GetParams(ctx)
	votePeriod := ms.VotePeriod(ctx)
	fmt.Println(votePeriod)

	aggregatePrevote, err := ms.GetAggregateValidatorPrevote(ctx, valAddr)
	if err != nil {
		fmt.Println("Error 4", err)
		return nil, sdkerrors.Wrap(types.ErrNoAggregatePrevote, msg.Validator)
	}

	fmt.Println("DFOGIJGSKJSD")
	fmt.Println("Block height", ctx.BlockHeight())
	fmt.Println("vote period", params.VotePeriod)
	fmt.Println("prevote ", aggregatePrevote.SubmitBlock)

	period := (uint64(ctx.BlockHeight()) / votePeriod) - (aggregatePrevote.SubmitBlock / votePeriod)
	fmt.Println(period)
	// Check a msg is submitted proper period
	if period != 1 {
		fmt.Println("AAAAA")
		return nil, types.ErrRevealPeriodMissMatch
	}

	fmt.Println("DDDDDDDDDDDD")
	validatorsList, err := types.ParseValidators(msg.ValidatorsAddress)
	if err != nil {
		fmt.Println("Error 5", err)
		return nil, err
	}
	fmt.Println("hahaahahahaahahah")
	// Verify a exchange rate with aggregate prevote hash
	hash := types.GetAggregateVoteHash(msg.Salt, msg.ValidatorsAddress, valAddr)
	if aggregatePrevote.Hash != hash.String() {
		return nil, sdkerrors.Wrapf(types.ErrVerificationFailed, "must be given %s not %s", aggregatePrevote.Hash, hash)
	}

	// Move aggregate prevote to aggregate vote with given exchange rates
	ms.SetAggregateValidatorVote(ctx, valAddr, types.NewAggregateValidatorVote(validatorsList, valAddr))
	ms.DeleteAggregateValidatorPrevote(ctx, valAddr)

	ctx.EventManager().EmitEvents(sdk.Events{
		sdk.NewEvent(
			types.EventTypeAggregateVote,
			sdk.NewAttribute(types.AttributeKeyVoter, msg.Validator),
			sdk.NewAttribute(types.AttributeKeyValidators, msg.ValidatorsAddress),
		),
		sdk.NewEvent(
			sdk.EventTypeMessage,
			sdk.NewAttribute(sdk.AttributeKeyModule, types.AttributeValueCategory),
			sdk.NewAttribute(sdk.AttributeKeySender, msg.Feeder),
		),
	})

	return &types.MsgAggregateValidatorVoteResponse{}, nil
}

func (ms msgServer) DelegateFeedConsent(goCtx context.Context, msg *types.MsgDelegateFeedConsent) (*types.MsgDelegateFeedConsentResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)

	operatorAddr, err := sdk.ValAddressFromBech32(msg.Operator)
	if err != nil {
		return nil, err
	}

	delegateAddr, err := sdk.AccAddressFromBech32(msg.Delegate)
	if err != nil {
		return nil, err
	}

	// Check the delegator is a validator
	val := ms.StakingKeeper.Validator(ctx, operatorAddr)
	if val == nil {
		return nil, sdkerrors.Wrap(stakingtypes.ErrNoValidatorFound, msg.Operator)
	}

	// Set the delegation
	ms.SetFeederDelegation(ctx, operatorAddr, delegateAddr)

	ctx.EventManager().EmitEvents(sdk.Events{
		sdk.NewEvent(
			types.EventTypeFeedDelegate,
			sdk.NewAttribute(types.AttributeKeyFeeder, msg.Delegate),
		),
		sdk.NewEvent(
			sdk.EventTypeMessage,
			sdk.NewAttribute(sdk.AttributeKeyModule, types.AttributeValueCategory),
			sdk.NewAttribute(sdk.AttributeKeySender, msg.Operator),
		),
	})

	return &types.MsgDelegateFeedConsentResponse{}, nil
}
