package keeper_test

import (
	"context"
	"testing"

	sdk "github.com/cosmos/cosmos-sdk/types"
	keepertest "github.com/edx04/oracleValidator/testutil/keeper"
	"github.com/edx04/oracleValidator/x/oraclevalidator/keeper"
	"github.com/edx04/oracleValidator/x/oraclevalidator/types"
)

func setupMsgServer(t testing.TB) (types.MsgServer, context.Context) {
	k, ctx := keepertest.OraclevalidatorKeeper(t)
	return keeper.NewMsgServerImpl(*k), sdk.WrapSDKContext(ctx)
}
