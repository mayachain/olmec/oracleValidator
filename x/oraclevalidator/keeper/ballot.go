package keeper

import (
	_ "fmt"
	_ "strings"

	sdk "github.com/cosmos/cosmos-sdk/types"
	_ "github.com/cosmos/cosmos-sdk/x/bank/types"
	"github.com/edx04/oracleValidator/x/oraclevalidator/types"
)

// OrganizeBallotByDenom collects all oracle votes for the period, categorized by the votes' denom parameter
func (k Keeper) OrganizeBallotByAddress(ctx sdk.Context, validatorClaimMap map[string]types.Claim) (votes map[string]types.AddressBallot) {
	votes = map[string]types.AddressBallot{}

	// Organize aggregate votes
	aggregateHandler := func(voterAddr sdk.ValAddress, vote types.AggregateValidatorVote) (stop bool) {
		// organize ballot only for the active validators
		claim, ok := validatorClaimMap[vote.Voter]

		if ok {
			power := claim.Power
			for _, validator := range vote.Validators {
				tmpPower := power

				votes[validator.Address] = append(votes[validator.Address],
					types.NewVoteForTally(
						validator.Address,
						voterAddr,
						tmpPower,
					),
				)
			}

		}

		return false
	}

	k.IterateAggregateValidatorVotes(ctx, aggregateHandler)

	// sort created ballot
	for address, ballot := range votes {
		votes[address] = ballot
	}

	return
}
