package keeper_test

import (
	"testing"

	testkeeper "github.com/edx04/oracleValidator/testutil/keeper"
	"github.com/edx04/oracleValidator/x/oraclevalidator/types"
	"github.com/stretchr/testify/require"
)

func TestGetParams(t *testing.T) {
	k, ctx := testkeeper.OraclevalidatorKeeper(t)
	params := types.DefaultParams()

	k.SetParams(ctx, params)

	require.EqualValues(t, params, k.GetParams(ctx))
}
