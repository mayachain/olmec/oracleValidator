package oraclevalidator

import (
	"fmt"

	sdk "github.com/cosmos/cosmos-sdk/types"
	"github.com/edx04/oracleValidator/x/oraclevalidator/keeper"
	"github.com/edx04/oracleValidator/x/oraclevalidator/types"
)

// InitGenesis initializes the capability module's state from a provided genesis
// state.
func InitGenesis(ctx sdk.Context, k keeper.Keeper, genState types.GenesisState) {
	// this line is used by starport scaffolding # genesis/module/init
	fmt.Println(genState)
	k.SetParams(ctx, genState.Params)
}

// ExportGenesis returns the capability module's exported genesis.
func ExportGenesis(ctx sdk.Context, k keeper.Keeper) *types.GenesisState {
	genesis := types.DefaultGenesis()
	genesis.Params = k.GetParams(ctx)

	// this line is used by starport scaffolding # genesis/module/export

	return genesis
}
