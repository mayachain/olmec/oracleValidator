package oraclevalidator

import (
	"fmt"
	"time"

	"github.com/cosmos/cosmos-sdk/telemetry"
	sdk "github.com/cosmos/cosmos-sdk/types"
	"github.com/edx04/oracleValidator/x/oraclevalidator/keeper"
	types "github.com/edx04/oracleValidator/x/oraclevalidator/types"
)

// EndBlocker is called at the end of every block
func EndBlocker(ctx sdk.Context, k keeper.Keeper) {
	defer telemetry.ModuleMeasureSince(types.ModuleName, time.Now(), telemetry.MetricKeyEndBlocker)

	params := k.GetParams(ctx)
	if types.IsPeriodLastBlock(ctx, params.VotePeriod) {

		// Build claim map over all validators in active set
		validatorClaimMap := make(map[string]types.Claim)

		maxValidators := k.StakingKeeper.MaxValidators(ctx)
		iterator := k.StakingKeeper.ValidatorsPowerStoreIterator(ctx)
		defer iterator.Close()

		powerReduction := k.StakingKeeper.PowerReduction(ctx)

		i := 0
		for ; iterator.Valid() && i < int(maxValidators); iterator.Next() {
			validator := k.StakingKeeper.Validator(ctx, iterator.Value())

			// Exclude not bonded validator
			if validator.IsBonded() {
				valAddr := validator.GetOperator()
				validatorClaimMap[valAddr.String()] = types.NewClaim(validator.GetConsensusPower(powerReduction), 0, 0, valAddr)
				i++
			}
		}

		// Clear all validators formwhitelist
		validators := k.IterateAddressWhitelist(ctx)

		for _, val := range validators {
			k.DeleteAddressFromWhitelist(ctx, val)

		}

		fmt.Println("validatorClaimMap", validatorClaimMap)

		voteMap := k.OrganizeBallotByAddress(ctx, validatorClaimMap)

		fmt.Println("voteMap", voteMap)

		//voteMapRT := voteMap.ToMap()

	}
}
