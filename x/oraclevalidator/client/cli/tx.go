package cli

import (
	"fmt"
	"strings"
	"time"

	"github.com/pkg/errors"
	"github.com/spf13/cobra"

	"github.com/cosmos/cosmos-sdk/client"
	"github.com/cosmos/cosmos-sdk/client/flags"
	"github.com/cosmos/cosmos-sdk/client/tx"
	sdk "github.com/cosmos/cosmos-sdk/types"

	// "github.com/cosmos/cosmos-sdk/client/flags"
	"github.com/edx04/oracleValidator/x/oraclevalidator/types"
)

var (
	DefaultRelativePacketTimeoutTimestamp = uint64((time.Duration(10) * time.Minute).Nanoseconds())
)

const (
	flagPacketTimeoutTimestamp = "packet-timeout-timestamp"
	listSeparator              = ","
)

// GetTxCmd returns the transaction commands for this module
func GetTxCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:                        types.ModuleName,
		Short:                      fmt.Sprintf("%s transactions subcommands", types.ModuleName),
		DisableFlagParsing:         true,
		SuggestionsMinimumDistance: 2,
		RunE:                       client.ValidateCmd,
	}

	// this line is used by starport scaffolding # 1
	cmd.AddCommand(GetCmdAggregateValidatorPrevote(), GetCmdAggregateValidatorVote())
	return cmd
}

// GetCmdAggregateValidatorPrevote will create a aggregateValidatorPrevote tx and sign it with the given key.
func GetCmdAggregateValidatorPrevote() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "aggregate-prevote [salt] [validators-whitelist] [validator]",
		Args:  cobra.RangeArgs(2, 3),
		Short: "Submit an oracle aggregate prevote for the validators whitelist",
		Long: strings.TrimSpace(`
Submit an oracle aggregate prevote for the validator's whitelist.
The purpose of aggregate prevote is to hide aggregate exchange rate vote with hash which is formatted 
as hex string in SHA256("{salt}:{validator},...,{validator}:{voter}")
.
`),
		RunE: func(cmd *cobra.Command, args []string) error {
			clientCtx, err := client.GetClientTxContext(cmd)
			if err != nil {
				return err
			}

			salt := args[0]
			ValidatorsStr := args[1]

			_, err = types.ParseValidators(ValidatorsStr)
			if err != nil {
				return fmt.Errorf("given validator {%s} is not a valid format; valdidator should be formatted as DecCoins; %s", ValidatorsStr, err.Error())
			}

			// Get from address
			voter := clientCtx.GetFromAddress()

			// By default the voter is voting on behalf of itself
			validator := sdk.ValAddress(voter)

			// Override validator if validator is given
			if len(args) == 3 {
				parsedVal, err := sdk.ValAddressFromBech32(args[2])
				if err != nil {
					return errors.Wrap(err, "validator address is invalid")
				}
				validator = parsedVal
			}

			hash := types.GetAggregateVoteHash(salt, ValidatorsStr, validator)
			msgs := []sdk.Msg{types.NewMsgAggregateValidatorPrevote(hash, voter, validator)}
			for _, msg := range msgs {
				if err := msg.ValidateBasic(); err != nil {
					return err
				}
			}

			return tx.GenerateOrBroadcastTxCLI(clientCtx, cmd.Flags(), msgs...)
		},
	}

	flags.AddTxFlagsToCmd(cmd)

	return cmd
}

// GetCmdAggregateValidatorVote will create a aggregateValidatorVote tx and sign it with the given key.
func GetCmdAggregateValidatorVote() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "aggregate-vote [salt] [valdiator] [validator]",
		Args:  cobra.RangeArgs(2, 3),
		Short: "Submit an oracle aggregate vote for the validators whitelist",
		Long: strings.TrimSpace(`
		Submit an oracle aggregate vote for the validator's whitelist.
		The purpose of aggregate vote is to hide aggregate exchange rate vote with hash which is formatted 
		as hex string in SHA256("{salt}:{validator},...,{validator}:{voter}")
`),
		RunE: func(cmd *cobra.Command, args []string) error {
			clientCtx, err := client.GetClientTxContext(cmd)
			if err != nil {
				return err
			}

			salt := args[0]
			ValidatorsStr := args[1]
			_, err = types.ParseValidators(ValidatorsStr)
			if err != nil {
				return fmt.Errorf("given validators {%s} is not a valid format; exchange rate should be formatted as DecCoin; %s", ValidatorsStr, err.Error())
			}

			// Get from address
			voter := clientCtx.GetFromAddress()

			// By default the voter is voting on behalf of itself
			validator := sdk.ValAddress(voter)

			// Override validator if validator is given
			if len(args) == 3 {
				parsedVal, err := sdk.ValAddressFromBech32(args[2])
				if err != nil {
					return errors.Wrap(err, "validator address is invalid")
				}
				validator = parsedVal
			}

			msgs := []sdk.Msg{types.NewMsgAggregateValidatorVote(salt, ValidatorsStr, voter, validator)}
			for _, msg := range msgs {
				if err := msg.ValidateBasic(); err != nil {
					return err
				}
			}

			return tx.GenerateOrBroadcastTxCLI(clientCtx, cmd.Flags(), msgs...)
		},
	}

	flags.AddTxFlagsToCmd(cmd)

	return cmd
}
