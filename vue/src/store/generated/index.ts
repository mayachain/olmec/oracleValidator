// THIS FILE IS GENERATED AUTOMATICALLY. DO NOT MODIFY.

import Edx04OracleValidatorEdx04OraclevalidatorOraclevalidator from './edx04/oracleValidator/edx04.oraclevalidator.oraclevalidator'


export default { 
  Edx04OracleValidatorEdx04OraclevalidatorOraclevalidator: load(Edx04OracleValidatorEdx04OraclevalidatorOraclevalidator, 'edx04.oraclevalidator.oraclevalidator'),
  
}


function load(mod, fullns) {
    return function init(store) {        
        if (store.hasModule([fullns])) {
            throw new Error('Duplicate module name detected: '+ fullns)
        }else{
            store.registerModule([fullns], mod)
            store.subscribe((mutation) => {
                if (mutation.type == 'common/env/INITIALIZE_WS_COMPLETE') {
                    store.dispatch(fullns+ '/init', null, {
                        root: true
                    })
                }
            })
        }
    }
}
