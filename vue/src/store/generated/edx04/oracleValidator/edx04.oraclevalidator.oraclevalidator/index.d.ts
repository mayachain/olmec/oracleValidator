import { FeederDelegation } from "./module/types/oraclevalidator/genesis";
import { Params } from "./module/types/oraclevalidator/params";
import { Validator } from "./module/types/oraclevalidator/params";
import { AggregateValidatorPrevote } from "./module/types/oraclevalidator/params";
import { AggregateValidatorVote } from "./module/types/oraclevalidator/params";
export { FeederDelegation, Params, Validator, AggregateValidatorPrevote, AggregateValidatorVote };
declare const _default;
export default _default;
