import { Reader, Writer } from "protobufjs/minimal";
import { Params, AggregateValidatorPrevote, AggregateValidatorVote } from "../oraclevalidator/params";
export declare const protobufPackage = "edx04.oraclevalidator.oraclevalidator";
/** QueryParamsRequest is request type for the Query/Params RPC method. */
export interface QueryParamsRequest {
}
/** QueryParamsResponse is response type for the Query/Params RPC method. */
export interface QueryParamsResponse {
    /** params holds all the parameters of this module. */
    params: Params | undefined;
}
/** QueryActivesRequest is the request type for the Query/Actives RPC method. */
export interface QueryActivesRequest {
}
/**
 * QueryActivesResponse is response type for the
 * Query/Actives RPC method.
 */
export interface QueryActivesResponse {
    /** actives defines a list of the validators which oracle aggreed to whitelist. */
    actives: string[];
}
/** QueryFeederDelegationRequest is the request type for the Query/FeederDelegation RPC method. */
export interface QueryFeederDelegationRequest {
    /** validator defines the validator address to query for. */
    validatorAddr: string;
}
/**
 * QueryFeederDelegationResponse is response type for the
 * Query/FeederDelegation RPC method.
 */
export interface QueryFeederDelegationResponse {
    /** feeder_addr defines the feeder delegation of a validator */
    feederAddr: string;
}
/** QueryAggregatePrevoteRequest is the request type for the Query/AggregatePrevote RPC method. */
export interface QueryAggregatePrevoteRequest {
    /** validator defines the validator address to query for. */
    validatorAddr: string;
}
/**
 * QueryAggregatePrevoteResponse is response type for the
 * Query/AggregatePrevote RPC method.
 */
export interface QueryAggregatePrevoteResponse {
    /** aggregate_prevote defines oracle aggregate prevote submitted by a validator in the current vote period */
    aggregatePrevote: AggregateValidatorPrevote | undefined;
}
/** QueryAggregatePrevotesRequest is the request type for the Query/AggregatePrevotes RPC method. */
export interface QueryAggregatePrevotesRequest {
}
/**
 * QueryAggregatePrevotesResponse is response type for the
 * Query/AggregatePrevotes RPC method.
 */
export interface QueryAggregatePrevotesResponse {
    /** aggregate_prevotes defines all oracle aggregate prevotes submitted in the current vote period */
    aggregatePrevotes: AggregateValidatorPrevote[];
}
/** QueryAggregateVoteRequest is the request type for the Query/AggregateVote RPC method. */
export interface QueryAggregateVoteRequest {
    /** validator defines the validator address to query for. */
    validatorAddr: string;
}
/**
 * QueryAggregateVoteResponse is response type for the
 * Query/AggregateVote RPC method.
 */
export interface QueryAggregateVoteResponse {
    /** aggregate_vote defines oracle aggregate vote submitted by a validator in the current vote period */
    aggregateVote: AggregateValidatorVote | undefined;
}
/** QueryAggregateVotesRequest is the request type for the Query/AggregateVotes RPC method. */
export interface QueryAggregateVotesRequest {
}
/**
 * QueryAggregateVotesResponse is response type for the
 * Query/AggregateVotes RPC method.
 */
export interface QueryAggregateVotesResponse {
    /** aggregate_votes defines all oracle aggregate votes submitted in the current vote period */
    aggregateVotes: AggregateValidatorVote[];
}
export declare const QueryParamsRequest: {
    encode(_: QueryParamsRequest, writer?: Writer): Writer;
    decode(input: Reader | Uint8Array, length?: number): QueryParamsRequest;
    fromJSON(_: any): QueryParamsRequest;
    toJSON(_: QueryParamsRequest): unknown;
    fromPartial(_: DeepPartial<QueryParamsRequest>): QueryParamsRequest;
};
export declare const QueryParamsResponse: {
    encode(message: QueryParamsResponse, writer?: Writer): Writer;
    decode(input: Reader | Uint8Array, length?: number): QueryParamsResponse;
    fromJSON(object: any): QueryParamsResponse;
    toJSON(message: QueryParamsResponse): unknown;
    fromPartial(object: DeepPartial<QueryParamsResponse>): QueryParamsResponse;
};
export declare const QueryActivesRequest: {
    encode(_: QueryActivesRequest, writer?: Writer): Writer;
    decode(input: Reader | Uint8Array, length?: number): QueryActivesRequest;
    fromJSON(_: any): QueryActivesRequest;
    toJSON(_: QueryActivesRequest): unknown;
    fromPartial(_: DeepPartial<QueryActivesRequest>): QueryActivesRequest;
};
export declare const QueryActivesResponse: {
    encode(message: QueryActivesResponse, writer?: Writer): Writer;
    decode(input: Reader | Uint8Array, length?: number): QueryActivesResponse;
    fromJSON(object: any): QueryActivesResponse;
    toJSON(message: QueryActivesResponse): unknown;
    fromPartial(object: DeepPartial<QueryActivesResponse>): QueryActivesResponse;
};
export declare const QueryFeederDelegationRequest: {
    encode(message: QueryFeederDelegationRequest, writer?: Writer): Writer;
    decode(input: Reader | Uint8Array, length?: number): QueryFeederDelegationRequest;
    fromJSON(object: any): QueryFeederDelegationRequest;
    toJSON(message: QueryFeederDelegationRequest): unknown;
    fromPartial(object: DeepPartial<QueryFeederDelegationRequest>): QueryFeederDelegationRequest;
};
export declare const QueryFeederDelegationResponse: {
    encode(message: QueryFeederDelegationResponse, writer?: Writer): Writer;
    decode(input: Reader | Uint8Array, length?: number): QueryFeederDelegationResponse;
    fromJSON(object: any): QueryFeederDelegationResponse;
    toJSON(message: QueryFeederDelegationResponse): unknown;
    fromPartial(object: DeepPartial<QueryFeederDelegationResponse>): QueryFeederDelegationResponse;
};
export declare const QueryAggregatePrevoteRequest: {
    encode(message: QueryAggregatePrevoteRequest, writer?: Writer): Writer;
    decode(input: Reader | Uint8Array, length?: number): QueryAggregatePrevoteRequest;
    fromJSON(object: any): QueryAggregatePrevoteRequest;
    toJSON(message: QueryAggregatePrevoteRequest): unknown;
    fromPartial(object: DeepPartial<QueryAggregatePrevoteRequest>): QueryAggregatePrevoteRequest;
};
export declare const QueryAggregatePrevoteResponse: {
    encode(message: QueryAggregatePrevoteResponse, writer?: Writer): Writer;
    decode(input: Reader | Uint8Array, length?: number): QueryAggregatePrevoteResponse;
    fromJSON(object: any): QueryAggregatePrevoteResponse;
    toJSON(message: QueryAggregatePrevoteResponse): unknown;
    fromPartial(object: DeepPartial<QueryAggregatePrevoteResponse>): QueryAggregatePrevoteResponse;
};
export declare const QueryAggregatePrevotesRequest: {
    encode(_: QueryAggregatePrevotesRequest, writer?: Writer): Writer;
    decode(input: Reader | Uint8Array, length?: number): QueryAggregatePrevotesRequest;
    fromJSON(_: any): QueryAggregatePrevotesRequest;
    toJSON(_: QueryAggregatePrevotesRequest): unknown;
    fromPartial(_: DeepPartial<QueryAggregatePrevotesRequest>): QueryAggregatePrevotesRequest;
};
export declare const QueryAggregatePrevotesResponse: {
    encode(message: QueryAggregatePrevotesResponse, writer?: Writer): Writer;
    decode(input: Reader | Uint8Array, length?: number): QueryAggregatePrevotesResponse;
    fromJSON(object: any): QueryAggregatePrevotesResponse;
    toJSON(message: QueryAggregatePrevotesResponse): unknown;
    fromPartial(object: DeepPartial<QueryAggregatePrevotesResponse>): QueryAggregatePrevotesResponse;
};
export declare const QueryAggregateVoteRequest: {
    encode(message: QueryAggregateVoteRequest, writer?: Writer): Writer;
    decode(input: Reader | Uint8Array, length?: number): QueryAggregateVoteRequest;
    fromJSON(object: any): QueryAggregateVoteRequest;
    toJSON(message: QueryAggregateVoteRequest): unknown;
    fromPartial(object: DeepPartial<QueryAggregateVoteRequest>): QueryAggregateVoteRequest;
};
export declare const QueryAggregateVoteResponse: {
    encode(message: QueryAggregateVoteResponse, writer?: Writer): Writer;
    decode(input: Reader | Uint8Array, length?: number): QueryAggregateVoteResponse;
    fromJSON(object: any): QueryAggregateVoteResponse;
    toJSON(message: QueryAggregateVoteResponse): unknown;
    fromPartial(object: DeepPartial<QueryAggregateVoteResponse>): QueryAggregateVoteResponse;
};
export declare const QueryAggregateVotesRequest: {
    encode(_: QueryAggregateVotesRequest, writer?: Writer): Writer;
    decode(input: Reader | Uint8Array, length?: number): QueryAggregateVotesRequest;
    fromJSON(_: any): QueryAggregateVotesRequest;
    toJSON(_: QueryAggregateVotesRequest): unknown;
    fromPartial(_: DeepPartial<QueryAggregateVotesRequest>): QueryAggregateVotesRequest;
};
export declare const QueryAggregateVotesResponse: {
    encode(message: QueryAggregateVotesResponse, writer?: Writer): Writer;
    decode(input: Reader | Uint8Array, length?: number): QueryAggregateVotesResponse;
    fromJSON(object: any): QueryAggregateVotesResponse;
    toJSON(message: QueryAggregateVotesResponse): unknown;
    fromPartial(object: DeepPartial<QueryAggregateVotesResponse>): QueryAggregateVotesResponse;
};
/** Query defines the gRPC querier service. */
export interface Query {
    /** Parameters queries the parameters of the module. */
    Params(request: QueryParamsRequest): Promise<QueryParamsResponse>;
    /** Actives returns all whitelisted validators */
    Actives(request: QueryActivesRequest): Promise<QueryActivesResponse>;
    /** FeederDelegation returns feeder delegation of a validator */
    FeederDelegation(request: QueryFeederDelegationRequest): Promise<QueryFeederDelegationResponse>;
    /** AggregatePrevote returns an aggregate prevote of a validator */
    AggregatePrevote(request: QueryAggregatePrevoteRequest): Promise<QueryAggregatePrevoteResponse>;
    /** AggregatePrevotes returns aggregate prevotes of all validators */
    AggregatePrevotes(request: QueryAggregatePrevotesRequest): Promise<QueryAggregatePrevotesResponse>;
    /** AggregateVote returns an aggregate vote of a validator */
    AggregateVote(request: QueryAggregateVoteRequest): Promise<QueryAggregateVoteResponse>;
    /** AggregateVotes returns aggregate votes of all validators */
    AggregateVotes(request: QueryAggregateVotesRequest): Promise<QueryAggregateVotesResponse>;
}
export declare class QueryClientImpl implements Query {
    private readonly rpc;
    constructor(rpc: Rpc);
    Params(request: QueryParamsRequest): Promise<QueryParamsResponse>;
    Actives(request: QueryActivesRequest): Promise<QueryActivesResponse>;
    FeederDelegation(request: QueryFeederDelegationRequest): Promise<QueryFeederDelegationResponse>;
    AggregatePrevote(request: QueryAggregatePrevoteRequest): Promise<QueryAggregatePrevoteResponse>;
    AggregatePrevotes(request: QueryAggregatePrevotesRequest): Promise<QueryAggregatePrevotesResponse>;
    AggregateVote(request: QueryAggregateVoteRequest): Promise<QueryAggregateVoteResponse>;
    AggregateVotes(request: QueryAggregateVotesRequest): Promise<QueryAggregateVotesResponse>;
}
interface Rpc {
    request(service: string, method: string, data: Uint8Array): Promise<Uint8Array>;
}
declare type Builtin = Date | Function | Uint8Array | string | number | undefined;
export declare type DeepPartial<T> = T extends Builtin ? T : T extends Array<infer U> ? Array<DeepPartial<U>> : T extends ReadonlyArray<infer U> ? ReadonlyArray<DeepPartial<U>> : T extends {} ? {
    [K in keyof T]?: DeepPartial<T[K]>;
} : Partial<T>;
export {};
