/* eslint-disable */
import * as Long from "long";
import { util, configure, Writer, Reader } from "protobufjs/minimal";
export const protobufPackage = "edx04.oraclevalidator.oraclevalidator";
const baseParams = { votePeriod: 0, voteThreshold: "" };
export const Params = {
    encode(message, writer = Writer.create()) {
        if (message.votePeriod !== 0) {
            writer.uint32(8).uint64(message.votePeriod);
        }
        if (message.voteThreshold !== "") {
            writer.uint32(18).string(message.voteThreshold);
        }
        for (const v of message.whitelist) {
            Validator.encode(v, writer.uint32(26).fork()).ldelim();
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = { ...baseParams };
        message.whitelist = [];
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.votePeriod = longToNumber(reader.uint64());
                    break;
                case 2:
                    message.voteThreshold = reader.string();
                    break;
                case 3:
                    message.whitelist.push(Validator.decode(reader, reader.uint32()));
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = { ...baseParams };
        message.whitelist = [];
        if (object.votePeriod !== undefined && object.votePeriod !== null) {
            message.votePeriod = Number(object.votePeriod);
        }
        else {
            message.votePeriod = 0;
        }
        if (object.voteThreshold !== undefined && object.voteThreshold !== null) {
            message.voteThreshold = String(object.voteThreshold);
        }
        else {
            message.voteThreshold = "";
        }
        if (object.whitelist !== undefined && object.whitelist !== null) {
            for (const e of object.whitelist) {
                message.whitelist.push(Validator.fromJSON(e));
            }
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.votePeriod !== undefined && (obj.votePeriod = message.votePeriod);
        message.voteThreshold !== undefined &&
            (obj.voteThreshold = message.voteThreshold);
        if (message.whitelist) {
            obj.whitelist = message.whitelist.map((e) => e ? Validator.toJSON(e) : undefined);
        }
        else {
            obj.whitelist = [];
        }
        return obj;
    },
    fromPartial(object) {
        const message = { ...baseParams };
        message.whitelist = [];
        if (object.votePeriod !== undefined && object.votePeriod !== null) {
            message.votePeriod = object.votePeriod;
        }
        else {
            message.votePeriod = 0;
        }
        if (object.voteThreshold !== undefined && object.voteThreshold !== null) {
            message.voteThreshold = object.voteThreshold;
        }
        else {
            message.voteThreshold = "";
        }
        if (object.whitelist !== undefined && object.whitelist !== null) {
            for (const e of object.whitelist) {
                message.whitelist.push(Validator.fromPartial(e));
            }
        }
        return message;
    },
};
const baseValidator = { address: "" };
export const Validator = {
    encode(message, writer = Writer.create()) {
        if (message.address !== "") {
            writer.uint32(10).string(message.address);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = { ...baseValidator };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.address = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = { ...baseValidator };
        if (object.address !== undefined && object.address !== null) {
            message.address = String(object.address);
        }
        else {
            message.address = "";
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.address !== undefined && (obj.address = message.address);
        return obj;
    },
    fromPartial(object) {
        const message = { ...baseValidator };
        if (object.address !== undefined && object.address !== null) {
            message.address = object.address;
        }
        else {
            message.address = "";
        }
        return message;
    },
};
const baseAggregateValidatorPrevote = {
    hash: "",
    voter: "",
    submitBlock: 0,
};
export const AggregateValidatorPrevote = {
    encode(message, writer = Writer.create()) {
        if (message.hash !== "") {
            writer.uint32(10).string(message.hash);
        }
        if (message.voter !== "") {
            writer.uint32(18).string(message.voter);
        }
        if (message.submitBlock !== 0) {
            writer.uint32(24).uint64(message.submitBlock);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = {
            ...baseAggregateValidatorPrevote,
        };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.hash = reader.string();
                    break;
                case 2:
                    message.voter = reader.string();
                    break;
                case 3:
                    message.submitBlock = longToNumber(reader.uint64());
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = {
            ...baseAggregateValidatorPrevote,
        };
        if (object.hash !== undefined && object.hash !== null) {
            message.hash = String(object.hash);
        }
        else {
            message.hash = "";
        }
        if (object.voter !== undefined && object.voter !== null) {
            message.voter = String(object.voter);
        }
        else {
            message.voter = "";
        }
        if (object.submitBlock !== undefined && object.submitBlock !== null) {
            message.submitBlock = Number(object.submitBlock);
        }
        else {
            message.submitBlock = 0;
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.hash !== undefined && (obj.hash = message.hash);
        message.voter !== undefined && (obj.voter = message.voter);
        message.submitBlock !== undefined &&
            (obj.submitBlock = message.submitBlock);
        return obj;
    },
    fromPartial(object) {
        const message = {
            ...baseAggregateValidatorPrevote,
        };
        if (object.hash !== undefined && object.hash !== null) {
            message.hash = object.hash;
        }
        else {
            message.hash = "";
        }
        if (object.voter !== undefined && object.voter !== null) {
            message.voter = object.voter;
        }
        else {
            message.voter = "";
        }
        if (object.submitBlock !== undefined && object.submitBlock !== null) {
            message.submitBlock = object.submitBlock;
        }
        else {
            message.submitBlock = 0;
        }
        return message;
    },
};
const baseAggregateValidatorVote = { voter: "" };
export const AggregateValidatorVote = {
    encode(message, writer = Writer.create()) {
        for (const v of message.validators) {
            Validator.encode(v, writer.uint32(10).fork()).ldelim();
        }
        if (message.voter !== "") {
            writer.uint32(18).string(message.voter);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = { ...baseAggregateValidatorVote };
        message.validators = [];
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.validators.push(Validator.decode(reader, reader.uint32()));
                    break;
                case 2:
                    message.voter = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = { ...baseAggregateValidatorVote };
        message.validators = [];
        if (object.validators !== undefined && object.validators !== null) {
            for (const e of object.validators) {
                message.validators.push(Validator.fromJSON(e));
            }
        }
        if (object.voter !== undefined && object.voter !== null) {
            message.voter = String(object.voter);
        }
        else {
            message.voter = "";
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        if (message.validators) {
            obj.validators = message.validators.map((e) => e ? Validator.toJSON(e) : undefined);
        }
        else {
            obj.validators = [];
        }
        message.voter !== undefined && (obj.voter = message.voter);
        return obj;
    },
    fromPartial(object) {
        const message = { ...baseAggregateValidatorVote };
        message.validators = [];
        if (object.validators !== undefined && object.validators !== null) {
            for (const e of object.validators) {
                message.validators.push(Validator.fromPartial(e));
            }
        }
        if (object.voter !== undefined && object.voter !== null) {
            message.voter = object.voter;
        }
        else {
            message.voter = "";
        }
        return message;
    },
};
var globalThis = (() => {
    if (typeof globalThis !== "undefined")
        return globalThis;
    if (typeof self !== "undefined")
        return self;
    if (typeof window !== "undefined")
        return window;
    if (typeof global !== "undefined")
        return global;
    throw "Unable to locate global object";
})();
function longToNumber(long) {
    if (long.gt(Number.MAX_SAFE_INTEGER)) {
        throw new globalThis.Error("Value is larger than Number.MAX_SAFE_INTEGER");
    }
    return long.toNumber();
}
if (util.Long !== Long) {
    util.Long = Long;
    configure();
}
