/* eslint-disable */
import { Reader, Writer } from "protobufjs/minimal";

export const protobufPackage = "edx04.oraclevalidator.oraclevalidator";

/**
 * MsgAggregateValidatorPrevote represents a message to submit
 * aggregate exchange rate prevote.
 */
export interface MsgAggregateValidatorPrevote {
  hash: string;
  feeder: string;
  validator: string;
}

/** MsgAggregateValidatorPrevoteResponse defines the Msg/AggregateValidatorPrevote response type. */
export interface MsgAggregateValidatorPrevoteResponse {}

/**
 * MsgAggregateValidatorVote represents a message to submit
 * aggregate exchange rate vote.
 */
export interface MsgAggregateValidatorVote {
  salt: string;
  validatorsAddress: string;
  feeder: string;
  validator: string;
}

/** MsgAggregateValidatorVoteResponse defines the Msg/AggregateExchangeRateVote response type. */
export interface MsgAggregateValidatorVoteResponse {}

/**
 * MsgDelegateFeedConsent represents a message to
 * delegate oracle voting rights to another address.
 */
export interface MsgDelegateFeedConsent {
  operator: string;
  delegate: string;
}

/** MsgDelegateFeedConsentResponse defines the Msg/DelegateFeedConsent response type. */
export interface MsgDelegateFeedConsentResponse {}

const baseMsgAggregateValidatorPrevote: object = {
  hash: "",
  feeder: "",
  validator: "",
};

export const MsgAggregateValidatorPrevote = {
  encode(
    message: MsgAggregateValidatorPrevote,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.hash !== "") {
      writer.uint32(10).string(message.hash);
    }
    if (message.feeder !== "") {
      writer.uint32(18).string(message.feeder);
    }
    if (message.validator !== "") {
      writer.uint32(26).string(message.validator);
    }
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgAggregateValidatorPrevote {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgAggregateValidatorPrevote,
    } as MsgAggregateValidatorPrevote;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.hash = reader.string();
          break;
        case 2:
          message.feeder = reader.string();
          break;
        case 3:
          message.validator = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgAggregateValidatorPrevote {
    const message = {
      ...baseMsgAggregateValidatorPrevote,
    } as MsgAggregateValidatorPrevote;
    if (object.hash !== undefined && object.hash !== null) {
      message.hash = String(object.hash);
    } else {
      message.hash = "";
    }
    if (object.feeder !== undefined && object.feeder !== null) {
      message.feeder = String(object.feeder);
    } else {
      message.feeder = "";
    }
    if (object.validator !== undefined && object.validator !== null) {
      message.validator = String(object.validator);
    } else {
      message.validator = "";
    }
    return message;
  },

  toJSON(message: MsgAggregateValidatorPrevote): unknown {
    const obj: any = {};
    message.hash !== undefined && (obj.hash = message.hash);
    message.feeder !== undefined && (obj.feeder = message.feeder);
    message.validator !== undefined && (obj.validator = message.validator);
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgAggregateValidatorPrevote>
  ): MsgAggregateValidatorPrevote {
    const message = {
      ...baseMsgAggregateValidatorPrevote,
    } as MsgAggregateValidatorPrevote;
    if (object.hash !== undefined && object.hash !== null) {
      message.hash = object.hash;
    } else {
      message.hash = "";
    }
    if (object.feeder !== undefined && object.feeder !== null) {
      message.feeder = object.feeder;
    } else {
      message.feeder = "";
    }
    if (object.validator !== undefined && object.validator !== null) {
      message.validator = object.validator;
    } else {
      message.validator = "";
    }
    return message;
  },
};

const baseMsgAggregateValidatorPrevoteResponse: object = {};

export const MsgAggregateValidatorPrevoteResponse = {
  encode(
    _: MsgAggregateValidatorPrevoteResponse,
    writer: Writer = Writer.create()
  ): Writer {
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgAggregateValidatorPrevoteResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgAggregateValidatorPrevoteResponse,
    } as MsgAggregateValidatorPrevoteResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): MsgAggregateValidatorPrevoteResponse {
    const message = {
      ...baseMsgAggregateValidatorPrevoteResponse,
    } as MsgAggregateValidatorPrevoteResponse;
    return message;
  },

  toJSON(_: MsgAggregateValidatorPrevoteResponse): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial(
    _: DeepPartial<MsgAggregateValidatorPrevoteResponse>
  ): MsgAggregateValidatorPrevoteResponse {
    const message = {
      ...baseMsgAggregateValidatorPrevoteResponse,
    } as MsgAggregateValidatorPrevoteResponse;
    return message;
  },
};

const baseMsgAggregateValidatorVote: object = {
  salt: "",
  validatorsAddress: "",
  feeder: "",
  validator: "",
};

export const MsgAggregateValidatorVote = {
  encode(
    message: MsgAggregateValidatorVote,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.salt !== "") {
      writer.uint32(10).string(message.salt);
    }
    if (message.validatorsAddress !== "") {
      writer.uint32(18).string(message.validatorsAddress);
    }
    if (message.feeder !== "") {
      writer.uint32(26).string(message.feeder);
    }
    if (message.validator !== "") {
      writer.uint32(34).string(message.validator);
    }
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgAggregateValidatorVote {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgAggregateValidatorVote,
    } as MsgAggregateValidatorVote;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.salt = reader.string();
          break;
        case 2:
          message.validatorsAddress = reader.string();
          break;
        case 3:
          message.feeder = reader.string();
          break;
        case 4:
          message.validator = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgAggregateValidatorVote {
    const message = {
      ...baseMsgAggregateValidatorVote,
    } as MsgAggregateValidatorVote;
    if (object.salt !== undefined && object.salt !== null) {
      message.salt = String(object.salt);
    } else {
      message.salt = "";
    }
    if (
      object.validatorsAddress !== undefined &&
      object.validatorsAddress !== null
    ) {
      message.validatorsAddress = String(object.validatorsAddress);
    } else {
      message.validatorsAddress = "";
    }
    if (object.feeder !== undefined && object.feeder !== null) {
      message.feeder = String(object.feeder);
    } else {
      message.feeder = "";
    }
    if (object.validator !== undefined && object.validator !== null) {
      message.validator = String(object.validator);
    } else {
      message.validator = "";
    }
    return message;
  },

  toJSON(message: MsgAggregateValidatorVote): unknown {
    const obj: any = {};
    message.salt !== undefined && (obj.salt = message.salt);
    message.validatorsAddress !== undefined &&
      (obj.validatorsAddress = message.validatorsAddress);
    message.feeder !== undefined && (obj.feeder = message.feeder);
    message.validator !== undefined && (obj.validator = message.validator);
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgAggregateValidatorVote>
  ): MsgAggregateValidatorVote {
    const message = {
      ...baseMsgAggregateValidatorVote,
    } as MsgAggregateValidatorVote;
    if (object.salt !== undefined && object.salt !== null) {
      message.salt = object.salt;
    } else {
      message.salt = "";
    }
    if (
      object.validatorsAddress !== undefined &&
      object.validatorsAddress !== null
    ) {
      message.validatorsAddress = object.validatorsAddress;
    } else {
      message.validatorsAddress = "";
    }
    if (object.feeder !== undefined && object.feeder !== null) {
      message.feeder = object.feeder;
    } else {
      message.feeder = "";
    }
    if (object.validator !== undefined && object.validator !== null) {
      message.validator = object.validator;
    } else {
      message.validator = "";
    }
    return message;
  },
};

const baseMsgAggregateValidatorVoteResponse: object = {};

export const MsgAggregateValidatorVoteResponse = {
  encode(
    _: MsgAggregateValidatorVoteResponse,
    writer: Writer = Writer.create()
  ): Writer {
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgAggregateValidatorVoteResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgAggregateValidatorVoteResponse,
    } as MsgAggregateValidatorVoteResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): MsgAggregateValidatorVoteResponse {
    const message = {
      ...baseMsgAggregateValidatorVoteResponse,
    } as MsgAggregateValidatorVoteResponse;
    return message;
  },

  toJSON(_: MsgAggregateValidatorVoteResponse): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial(
    _: DeepPartial<MsgAggregateValidatorVoteResponse>
  ): MsgAggregateValidatorVoteResponse {
    const message = {
      ...baseMsgAggregateValidatorVoteResponse,
    } as MsgAggregateValidatorVoteResponse;
    return message;
  },
};

const baseMsgDelegateFeedConsent: object = { operator: "", delegate: "" };

export const MsgDelegateFeedConsent = {
  encode(
    message: MsgDelegateFeedConsent,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.operator !== "") {
      writer.uint32(10).string(message.operator);
    }
    if (message.delegate !== "") {
      writer.uint32(18).string(message.delegate);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): MsgDelegateFeedConsent {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseMsgDelegateFeedConsent } as MsgDelegateFeedConsent;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.operator = reader.string();
          break;
        case 2:
          message.delegate = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgDelegateFeedConsent {
    const message = { ...baseMsgDelegateFeedConsent } as MsgDelegateFeedConsent;
    if (object.operator !== undefined && object.operator !== null) {
      message.operator = String(object.operator);
    } else {
      message.operator = "";
    }
    if (object.delegate !== undefined && object.delegate !== null) {
      message.delegate = String(object.delegate);
    } else {
      message.delegate = "";
    }
    return message;
  },

  toJSON(message: MsgDelegateFeedConsent): unknown {
    const obj: any = {};
    message.operator !== undefined && (obj.operator = message.operator);
    message.delegate !== undefined && (obj.delegate = message.delegate);
    return obj;
  },

  fromPartial(
    object: DeepPartial<MsgDelegateFeedConsent>
  ): MsgDelegateFeedConsent {
    const message = { ...baseMsgDelegateFeedConsent } as MsgDelegateFeedConsent;
    if (object.operator !== undefined && object.operator !== null) {
      message.operator = object.operator;
    } else {
      message.operator = "";
    }
    if (object.delegate !== undefined && object.delegate !== null) {
      message.delegate = object.delegate;
    } else {
      message.delegate = "";
    }
    return message;
  },
};

const baseMsgDelegateFeedConsentResponse: object = {};

export const MsgDelegateFeedConsentResponse = {
  encode(
    _: MsgDelegateFeedConsentResponse,
    writer: Writer = Writer.create()
  ): Writer {
    return writer;
  },

  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgDelegateFeedConsentResponse {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseMsgDelegateFeedConsentResponse,
    } as MsgDelegateFeedConsentResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): MsgDelegateFeedConsentResponse {
    const message = {
      ...baseMsgDelegateFeedConsentResponse,
    } as MsgDelegateFeedConsentResponse;
    return message;
  },

  toJSON(_: MsgDelegateFeedConsentResponse): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial(
    _: DeepPartial<MsgDelegateFeedConsentResponse>
  ): MsgDelegateFeedConsentResponse {
    const message = {
      ...baseMsgDelegateFeedConsentResponse,
    } as MsgDelegateFeedConsentResponse;
    return message;
  },
};

/** Msg defines the Msg service. */
export interface Msg {
  /**
   * this line is used by starport scaffolding # proto/tx/rpc
   * AggregateValidatorPrevote defines a method for submitting
   * aggregate validator prevote
   */
  AggregateValidatorPrevote(
    request: MsgAggregateValidatorPrevote
  ): Promise<MsgAggregateValidatorPrevoteResponse>;
  /**
   * AggregateValidatorVote defines a method for submitting
   * aggregate exchange rate vote
   */
  AggregateValidatorVote(
    request: MsgAggregateValidatorVote
  ): Promise<MsgAggregateValidatorVoteResponse>;
  /** DelegateFeedConsent defines a method for setting the feeder delegation */
  DelegateFeedConsent(
    request: MsgDelegateFeedConsent
  ): Promise<MsgDelegateFeedConsentResponse>;
}

export class MsgClientImpl implements Msg {
  private readonly rpc: Rpc;
  constructor(rpc: Rpc) {
    this.rpc = rpc;
  }
  AggregateValidatorPrevote(
    request: MsgAggregateValidatorPrevote
  ): Promise<MsgAggregateValidatorPrevoteResponse> {
    const data = MsgAggregateValidatorPrevote.encode(request).finish();
    const promise = this.rpc.request(
      "edx04.oraclevalidator.oraclevalidator.Msg",
      "AggregateValidatorPrevote",
      data
    );
    return promise.then((data) =>
      MsgAggregateValidatorPrevoteResponse.decode(new Reader(data))
    );
  }

  AggregateValidatorVote(
    request: MsgAggregateValidatorVote
  ): Promise<MsgAggregateValidatorVoteResponse> {
    const data = MsgAggregateValidatorVote.encode(request).finish();
    const promise = this.rpc.request(
      "edx04.oraclevalidator.oraclevalidator.Msg",
      "AggregateValidatorVote",
      data
    );
    return promise.then((data) =>
      MsgAggregateValidatorVoteResponse.decode(new Reader(data))
    );
  }

  DelegateFeedConsent(
    request: MsgDelegateFeedConsent
  ): Promise<MsgDelegateFeedConsentResponse> {
    const data = MsgDelegateFeedConsent.encode(request).finish();
    const promise = this.rpc.request(
      "edx04.oraclevalidator.oraclevalidator.Msg",
      "DelegateFeedConsent",
      data
    );
    return promise.then((data) =>
      MsgDelegateFeedConsentResponse.decode(new Reader(data))
    );
  }
}

interface Rpc {
  request(
    service: string,
    method: string,
    data: Uint8Array
  ): Promise<Uint8Array>;
}

type Builtin = Date | Function | Uint8Array | string | number | undefined;
export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends {}
  ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;
