import { Reader, Writer } from "protobufjs/minimal";
export declare const protobufPackage = "edx04.oraclevalidator.oraclevalidator";
/**
 * MsgAggregateValidatorPrevote represents a message to submit
 * aggregate exchange rate prevote.
 */
export interface MsgAggregateValidatorPrevote {
    hash: string;
    feeder: string;
    validator: string;
}
/** MsgAggregateValidatorPrevoteResponse defines the Msg/AggregateValidatorPrevote response type. */
export interface MsgAggregateValidatorPrevoteResponse {
}
/**
 * MsgAggregateValidatorVote represents a message to submit
 * aggregate exchange rate vote.
 */
export interface MsgAggregateValidatorVote {
    salt: string;
    validatorsAddress: string;
    feeder: string;
    validator: string;
}
/** MsgAggregateValidatorVoteResponse defines the Msg/AggregateExchangeRateVote response type. */
export interface MsgAggregateValidatorVoteResponse {
}
/**
 * MsgDelegateFeedConsent represents a message to
 * delegate oracle voting rights to another address.
 */
export interface MsgDelegateFeedConsent {
    operator: string;
    delegate: string;
}
/** MsgDelegateFeedConsentResponse defines the Msg/DelegateFeedConsent response type. */
export interface MsgDelegateFeedConsentResponse {
}
export declare const MsgAggregateValidatorPrevote: {
    encode(message: MsgAggregateValidatorPrevote, writer?: Writer): Writer;
    decode(input: Reader | Uint8Array, length?: number): MsgAggregateValidatorPrevote;
    fromJSON(object: any): MsgAggregateValidatorPrevote;
    toJSON(message: MsgAggregateValidatorPrevote): unknown;
    fromPartial(object: DeepPartial<MsgAggregateValidatorPrevote>): MsgAggregateValidatorPrevote;
};
export declare const MsgAggregateValidatorPrevoteResponse: {
    encode(_: MsgAggregateValidatorPrevoteResponse, writer?: Writer): Writer;
    decode(input: Reader | Uint8Array, length?: number): MsgAggregateValidatorPrevoteResponse;
    fromJSON(_: any): MsgAggregateValidatorPrevoteResponse;
    toJSON(_: MsgAggregateValidatorPrevoteResponse): unknown;
    fromPartial(_: DeepPartial<MsgAggregateValidatorPrevoteResponse>): MsgAggregateValidatorPrevoteResponse;
};
export declare const MsgAggregateValidatorVote: {
    encode(message: MsgAggregateValidatorVote, writer?: Writer): Writer;
    decode(input: Reader | Uint8Array, length?: number): MsgAggregateValidatorVote;
    fromJSON(object: any): MsgAggregateValidatorVote;
    toJSON(message: MsgAggregateValidatorVote): unknown;
    fromPartial(object: DeepPartial<MsgAggregateValidatorVote>): MsgAggregateValidatorVote;
};
export declare const MsgAggregateValidatorVoteResponse: {
    encode(_: MsgAggregateValidatorVoteResponse, writer?: Writer): Writer;
    decode(input: Reader | Uint8Array, length?: number): MsgAggregateValidatorVoteResponse;
    fromJSON(_: any): MsgAggregateValidatorVoteResponse;
    toJSON(_: MsgAggregateValidatorVoteResponse): unknown;
    fromPartial(_: DeepPartial<MsgAggregateValidatorVoteResponse>): MsgAggregateValidatorVoteResponse;
};
export declare const MsgDelegateFeedConsent: {
    encode(message: MsgDelegateFeedConsent, writer?: Writer): Writer;
    decode(input: Reader | Uint8Array, length?: number): MsgDelegateFeedConsent;
    fromJSON(object: any): MsgDelegateFeedConsent;
    toJSON(message: MsgDelegateFeedConsent): unknown;
    fromPartial(object: DeepPartial<MsgDelegateFeedConsent>): MsgDelegateFeedConsent;
};
export declare const MsgDelegateFeedConsentResponse: {
    encode(_: MsgDelegateFeedConsentResponse, writer?: Writer): Writer;
    decode(input: Reader | Uint8Array, length?: number): MsgDelegateFeedConsentResponse;
    fromJSON(_: any): MsgDelegateFeedConsentResponse;
    toJSON(_: MsgDelegateFeedConsentResponse): unknown;
    fromPartial(_: DeepPartial<MsgDelegateFeedConsentResponse>): MsgDelegateFeedConsentResponse;
};
/** Msg defines the Msg service. */
export interface Msg {
    /**
     * this line is used by starport scaffolding # proto/tx/rpc
     * AggregateValidatorPrevote defines a method for submitting
     * aggregate validator prevote
     */
    AggregateValidatorPrevote(request: MsgAggregateValidatorPrevote): Promise<MsgAggregateValidatorPrevoteResponse>;
    /**
     * AggregateValidatorVote defines a method for submitting
     * aggregate exchange rate vote
     */
    AggregateValidatorVote(request: MsgAggregateValidatorVote): Promise<MsgAggregateValidatorVoteResponse>;
    /** DelegateFeedConsent defines a method for setting the feeder delegation */
    DelegateFeedConsent(request: MsgDelegateFeedConsent): Promise<MsgDelegateFeedConsentResponse>;
}
export declare class MsgClientImpl implements Msg {
    private readonly rpc;
    constructor(rpc: Rpc);
    AggregateValidatorPrevote(request: MsgAggregateValidatorPrevote): Promise<MsgAggregateValidatorPrevoteResponse>;
    AggregateValidatorVote(request: MsgAggregateValidatorVote): Promise<MsgAggregateValidatorVoteResponse>;
    DelegateFeedConsent(request: MsgDelegateFeedConsent): Promise<MsgDelegateFeedConsentResponse>;
}
interface Rpc {
    request(service: string, method: string, data: Uint8Array): Promise<Uint8Array>;
}
declare type Builtin = Date | Function | Uint8Array | string | number | undefined;
export declare type DeepPartial<T> = T extends Builtin ? T : T extends Array<infer U> ? Array<DeepPartial<U>> : T extends ReadonlyArray<infer U> ? ReadonlyArray<DeepPartial<U>> : T extends {} ? {
    [K in keyof T]?: DeepPartial<T[K]>;
} : Partial<T>;
export {};
