/* eslint-disable */
import { Params, Validator } from "../oraclevalidator/params";
import { Writer, Reader } from "protobufjs/minimal";
export const protobufPackage = "edx04.oraclevalidator.oraclevalidator";
const baseGenesisState = {};
export const GenesisState = {
    encode(message, writer = Writer.create()) {
        if (message.params !== undefined) {
            Params.encode(message.params, writer.uint32(10).fork()).ldelim();
        }
        for (const v of message.whitelistValidators) {
            Validator.encode(v, writer.uint32(18).fork()).ldelim();
        }
        for (const v of message.feederDelegations) {
            FeederDelegation.encode(v, writer.uint32(26).fork()).ldelim();
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = { ...baseGenesisState };
        message.whitelistValidators = [];
        message.feederDelegations = [];
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.params = Params.decode(reader, reader.uint32());
                    break;
                case 2:
                    message.whitelistValidators.push(Validator.decode(reader, reader.uint32()));
                    break;
                case 3:
                    message.feederDelegations.push(FeederDelegation.decode(reader, reader.uint32()));
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = { ...baseGenesisState };
        message.whitelistValidators = [];
        message.feederDelegations = [];
        if (object.params !== undefined && object.params !== null) {
            message.params = Params.fromJSON(object.params);
        }
        else {
            message.params = undefined;
        }
        if (object.whitelistValidators !== undefined &&
            object.whitelistValidators !== null) {
            for (const e of object.whitelistValidators) {
                message.whitelistValidators.push(Validator.fromJSON(e));
            }
        }
        if (object.feederDelegations !== undefined &&
            object.feederDelegations !== null) {
            for (const e of object.feederDelegations) {
                message.feederDelegations.push(FeederDelegation.fromJSON(e));
            }
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.params !== undefined &&
            (obj.params = message.params ? Params.toJSON(message.params) : undefined);
        if (message.whitelistValidators) {
            obj.whitelistValidators = message.whitelistValidators.map((e) => e ? Validator.toJSON(e) : undefined);
        }
        else {
            obj.whitelistValidators = [];
        }
        if (message.feederDelegations) {
            obj.feederDelegations = message.feederDelegations.map((e) => e ? FeederDelegation.toJSON(e) : undefined);
        }
        else {
            obj.feederDelegations = [];
        }
        return obj;
    },
    fromPartial(object) {
        const message = { ...baseGenesisState };
        message.whitelistValidators = [];
        message.feederDelegations = [];
        if (object.params !== undefined && object.params !== null) {
            message.params = Params.fromPartial(object.params);
        }
        else {
            message.params = undefined;
        }
        if (object.whitelistValidators !== undefined &&
            object.whitelistValidators !== null) {
            for (const e of object.whitelistValidators) {
                message.whitelistValidators.push(Validator.fromPartial(e));
            }
        }
        if (object.feederDelegations !== undefined &&
            object.feederDelegations !== null) {
            for (const e of object.feederDelegations) {
                message.feederDelegations.push(FeederDelegation.fromPartial(e));
            }
        }
        return message;
    },
};
const baseFeederDelegation = {
    feederAddress: "",
    validatorAddress: "",
};
export const FeederDelegation = {
    encode(message, writer = Writer.create()) {
        if (message.feederAddress !== "") {
            writer.uint32(10).string(message.feederAddress);
        }
        if (message.validatorAddress !== "") {
            writer.uint32(18).string(message.validatorAddress);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = { ...baseFeederDelegation };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.feederAddress = reader.string();
                    break;
                case 2:
                    message.validatorAddress = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = { ...baseFeederDelegation };
        if (object.feederAddress !== undefined && object.feederAddress !== null) {
            message.feederAddress = String(object.feederAddress);
        }
        else {
            message.feederAddress = "";
        }
        if (object.validatorAddress !== undefined &&
            object.validatorAddress !== null) {
            message.validatorAddress = String(object.validatorAddress);
        }
        else {
            message.validatorAddress = "";
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.feederAddress !== undefined &&
            (obj.feederAddress = message.feederAddress);
        message.validatorAddress !== undefined &&
            (obj.validatorAddress = message.validatorAddress);
        return obj;
    },
    fromPartial(object) {
        const message = { ...baseFeederDelegation };
        if (object.feederAddress !== undefined && object.feederAddress !== null) {
            message.feederAddress = object.feederAddress;
        }
        else {
            message.feederAddress = "";
        }
        if (object.validatorAddress !== undefined &&
            object.validatorAddress !== null) {
            message.validatorAddress = object.validatorAddress;
        }
        else {
            message.validatorAddress = "";
        }
        return message;
    },
};
