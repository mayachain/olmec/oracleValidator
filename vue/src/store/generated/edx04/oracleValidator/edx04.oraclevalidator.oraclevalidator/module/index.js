// THIS FILE IS GENERATED AUTOMATICALLY. DO NOT MODIFY.
import { SigningStargateClient } from "@cosmjs/stargate";
import { Registry } from "@cosmjs/proto-signing";
import { Api } from "./rest";
import { MsgAggregateValidatorVote } from "./types/oraclevalidator/tx";
import { MsgAggregateValidatorPrevote } from "./types/oraclevalidator/tx";
import { MsgDelegateFeedConsent } from "./types/oraclevalidator/tx";
const types = [
    ["/edx04.oraclevalidator.oraclevalidator.MsgAggregateValidatorVote", MsgAggregateValidatorVote],
    ["/edx04.oraclevalidator.oraclevalidator.MsgAggregateValidatorPrevote", MsgAggregateValidatorPrevote],
    ["/edx04.oraclevalidator.oraclevalidator.MsgDelegateFeedConsent", MsgDelegateFeedConsent],
];
export const MissingWalletError = new Error("wallet is required");
export const registry = new Registry(types);
const defaultFee = {
    amount: [],
    gas: "200000",
};
const txClient = async (wallet, { addr: addr } = { addr: "http://localhost:26657" }) => {
    if (!wallet)
        throw MissingWalletError;
    let client;
    if (addr) {
        client = await SigningStargateClient.connectWithSigner(addr, wallet, { registry });
    }
    else {
        client = await SigningStargateClient.offline(wallet, { registry });
    }
    const { address } = (await wallet.getAccounts())[0];
    return {
        signAndBroadcast: (msgs, { fee, memo } = { fee: defaultFee, memo: "" }) => client.signAndBroadcast(address, msgs, fee, memo),
        msgAggregateValidatorVote: (data) => ({ typeUrl: "/edx04.oraclevalidator.oraclevalidator.MsgAggregateValidatorVote", value: MsgAggregateValidatorVote.fromPartial(data) }),
        msgAggregateValidatorPrevote: (data) => ({ typeUrl: "/edx04.oraclevalidator.oraclevalidator.MsgAggregateValidatorPrevote", value: MsgAggregateValidatorPrevote.fromPartial(data) }),
        msgDelegateFeedConsent: (data) => ({ typeUrl: "/edx04.oraclevalidator.oraclevalidator.MsgDelegateFeedConsent", value: MsgDelegateFeedConsent.fromPartial(data) }),
    };
};
const queryClient = async ({ addr: addr } = { addr: "http://localhost:1317" }) => {
    return new Api({ baseUrl: addr });
};
export { txClient, queryClient, };
