import { Writer, Reader } from "protobufjs/minimal";
export declare const protobufPackage = "edx04.oraclevalidator.oraclevalidator";
/** Params defines the parameters for the module. */
export interface Params {
    votePeriod: number;
    voteThreshold: string;
    whitelist: Validator[];
}
export interface Validator {
    address: string;
}
export interface AggregateValidatorPrevote {
    hash: string;
    voter: string;
    submitBlock: number;
}
export interface AggregateValidatorVote {
    validators: Validator[];
    voter: string;
}
export declare const Params: {
    encode(message: Params, writer?: Writer): Writer;
    decode(input: Reader | Uint8Array, length?: number): Params;
    fromJSON(object: any): Params;
    toJSON(message: Params): unknown;
    fromPartial(object: DeepPartial<Params>): Params;
};
export declare const Validator: {
    encode(message: Validator, writer?: Writer): Writer;
    decode(input: Reader | Uint8Array, length?: number): Validator;
    fromJSON(object: any): Validator;
    toJSON(message: Validator): unknown;
    fromPartial(object: DeepPartial<Validator>): Validator;
};
export declare const AggregateValidatorPrevote: {
    encode(message: AggregateValidatorPrevote, writer?: Writer): Writer;
    decode(input: Reader | Uint8Array, length?: number): AggregateValidatorPrevote;
    fromJSON(object: any): AggregateValidatorPrevote;
    toJSON(message: AggregateValidatorPrevote): unknown;
    fromPartial(object: DeepPartial<AggregateValidatorPrevote>): AggregateValidatorPrevote;
};
export declare const AggregateValidatorVote: {
    encode(message: AggregateValidatorVote, writer?: Writer): Writer;
    decode(input: Reader | Uint8Array, length?: number): AggregateValidatorVote;
    fromJSON(object: any): AggregateValidatorVote;
    toJSON(message: AggregateValidatorVote): unknown;
    fromPartial(object: DeepPartial<AggregateValidatorVote>): AggregateValidatorVote;
};
declare type Builtin = Date | Function | Uint8Array | string | number | undefined;
export declare type DeepPartial<T> = T extends Builtin ? T : T extends Array<infer U> ? Array<DeepPartial<U>> : T extends ReadonlyArray<infer U> ? ReadonlyArray<DeepPartial<U>> : T extends {} ? {
    [K in keyof T]?: DeepPartial<T[K]>;
} : Partial<T>;
export {};
