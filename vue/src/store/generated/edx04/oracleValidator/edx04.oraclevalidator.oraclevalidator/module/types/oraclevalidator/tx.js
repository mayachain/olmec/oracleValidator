/* eslint-disable */
import { Reader, Writer } from "protobufjs/minimal";
export const protobufPackage = "edx04.oraclevalidator.oraclevalidator";
const baseMsgAggregateValidatorPrevote = {
    hash: "",
    feeder: "",
    validator: "",
};
export const MsgAggregateValidatorPrevote = {
    encode(message, writer = Writer.create()) {
        if (message.hash !== "") {
            writer.uint32(10).string(message.hash);
        }
        if (message.feeder !== "") {
            writer.uint32(18).string(message.feeder);
        }
        if (message.validator !== "") {
            writer.uint32(26).string(message.validator);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = {
            ...baseMsgAggregateValidatorPrevote,
        };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.hash = reader.string();
                    break;
                case 2:
                    message.feeder = reader.string();
                    break;
                case 3:
                    message.validator = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = {
            ...baseMsgAggregateValidatorPrevote,
        };
        if (object.hash !== undefined && object.hash !== null) {
            message.hash = String(object.hash);
        }
        else {
            message.hash = "";
        }
        if (object.feeder !== undefined && object.feeder !== null) {
            message.feeder = String(object.feeder);
        }
        else {
            message.feeder = "";
        }
        if (object.validator !== undefined && object.validator !== null) {
            message.validator = String(object.validator);
        }
        else {
            message.validator = "";
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.hash !== undefined && (obj.hash = message.hash);
        message.feeder !== undefined && (obj.feeder = message.feeder);
        message.validator !== undefined && (obj.validator = message.validator);
        return obj;
    },
    fromPartial(object) {
        const message = {
            ...baseMsgAggregateValidatorPrevote,
        };
        if (object.hash !== undefined && object.hash !== null) {
            message.hash = object.hash;
        }
        else {
            message.hash = "";
        }
        if (object.feeder !== undefined && object.feeder !== null) {
            message.feeder = object.feeder;
        }
        else {
            message.feeder = "";
        }
        if (object.validator !== undefined && object.validator !== null) {
            message.validator = object.validator;
        }
        else {
            message.validator = "";
        }
        return message;
    },
};
const baseMsgAggregateValidatorPrevoteResponse = {};
export const MsgAggregateValidatorPrevoteResponse = {
    encode(_, writer = Writer.create()) {
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = {
            ...baseMsgAggregateValidatorPrevoteResponse,
        };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(_) {
        const message = {
            ...baseMsgAggregateValidatorPrevoteResponse,
        };
        return message;
    },
    toJSON(_) {
        const obj = {};
        return obj;
    },
    fromPartial(_) {
        const message = {
            ...baseMsgAggregateValidatorPrevoteResponse,
        };
        return message;
    },
};
const baseMsgAggregateValidatorVote = {
    salt: "",
    validatorsAddress: "",
    feeder: "",
    validator: "",
};
export const MsgAggregateValidatorVote = {
    encode(message, writer = Writer.create()) {
        if (message.salt !== "") {
            writer.uint32(10).string(message.salt);
        }
        if (message.validatorsAddress !== "") {
            writer.uint32(18).string(message.validatorsAddress);
        }
        if (message.feeder !== "") {
            writer.uint32(26).string(message.feeder);
        }
        if (message.validator !== "") {
            writer.uint32(34).string(message.validator);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = {
            ...baseMsgAggregateValidatorVote,
        };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.salt = reader.string();
                    break;
                case 2:
                    message.validatorsAddress = reader.string();
                    break;
                case 3:
                    message.feeder = reader.string();
                    break;
                case 4:
                    message.validator = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = {
            ...baseMsgAggregateValidatorVote,
        };
        if (object.salt !== undefined && object.salt !== null) {
            message.salt = String(object.salt);
        }
        else {
            message.salt = "";
        }
        if (object.validatorsAddress !== undefined &&
            object.validatorsAddress !== null) {
            message.validatorsAddress = String(object.validatorsAddress);
        }
        else {
            message.validatorsAddress = "";
        }
        if (object.feeder !== undefined && object.feeder !== null) {
            message.feeder = String(object.feeder);
        }
        else {
            message.feeder = "";
        }
        if (object.validator !== undefined && object.validator !== null) {
            message.validator = String(object.validator);
        }
        else {
            message.validator = "";
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.salt !== undefined && (obj.salt = message.salt);
        message.validatorsAddress !== undefined &&
            (obj.validatorsAddress = message.validatorsAddress);
        message.feeder !== undefined && (obj.feeder = message.feeder);
        message.validator !== undefined && (obj.validator = message.validator);
        return obj;
    },
    fromPartial(object) {
        const message = {
            ...baseMsgAggregateValidatorVote,
        };
        if (object.salt !== undefined && object.salt !== null) {
            message.salt = object.salt;
        }
        else {
            message.salt = "";
        }
        if (object.validatorsAddress !== undefined &&
            object.validatorsAddress !== null) {
            message.validatorsAddress = object.validatorsAddress;
        }
        else {
            message.validatorsAddress = "";
        }
        if (object.feeder !== undefined && object.feeder !== null) {
            message.feeder = object.feeder;
        }
        else {
            message.feeder = "";
        }
        if (object.validator !== undefined && object.validator !== null) {
            message.validator = object.validator;
        }
        else {
            message.validator = "";
        }
        return message;
    },
};
const baseMsgAggregateValidatorVoteResponse = {};
export const MsgAggregateValidatorVoteResponse = {
    encode(_, writer = Writer.create()) {
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = {
            ...baseMsgAggregateValidatorVoteResponse,
        };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(_) {
        const message = {
            ...baseMsgAggregateValidatorVoteResponse,
        };
        return message;
    },
    toJSON(_) {
        const obj = {};
        return obj;
    },
    fromPartial(_) {
        const message = {
            ...baseMsgAggregateValidatorVoteResponse,
        };
        return message;
    },
};
const baseMsgDelegateFeedConsent = { operator: "", delegate: "" };
export const MsgDelegateFeedConsent = {
    encode(message, writer = Writer.create()) {
        if (message.operator !== "") {
            writer.uint32(10).string(message.operator);
        }
        if (message.delegate !== "") {
            writer.uint32(18).string(message.delegate);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = { ...baseMsgDelegateFeedConsent };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.operator = reader.string();
                    break;
                case 2:
                    message.delegate = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = { ...baseMsgDelegateFeedConsent };
        if (object.operator !== undefined && object.operator !== null) {
            message.operator = String(object.operator);
        }
        else {
            message.operator = "";
        }
        if (object.delegate !== undefined && object.delegate !== null) {
            message.delegate = String(object.delegate);
        }
        else {
            message.delegate = "";
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.operator !== undefined && (obj.operator = message.operator);
        message.delegate !== undefined && (obj.delegate = message.delegate);
        return obj;
    },
    fromPartial(object) {
        const message = { ...baseMsgDelegateFeedConsent };
        if (object.operator !== undefined && object.operator !== null) {
            message.operator = object.operator;
        }
        else {
            message.operator = "";
        }
        if (object.delegate !== undefined && object.delegate !== null) {
            message.delegate = object.delegate;
        }
        else {
            message.delegate = "";
        }
        return message;
    },
};
const baseMsgDelegateFeedConsentResponse = {};
export const MsgDelegateFeedConsentResponse = {
    encode(_, writer = Writer.create()) {
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = {
            ...baseMsgDelegateFeedConsentResponse,
        };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(_) {
        const message = {
            ...baseMsgDelegateFeedConsentResponse,
        };
        return message;
    },
    toJSON(_) {
        const obj = {};
        return obj;
    },
    fromPartial(_) {
        const message = {
            ...baseMsgDelegateFeedConsentResponse,
        };
        return message;
    },
};
export class MsgClientImpl {
    constructor(rpc) {
        this.rpc = rpc;
    }
    AggregateValidatorPrevote(request) {
        const data = MsgAggregateValidatorPrevote.encode(request).finish();
        const promise = this.rpc.request("edx04.oraclevalidator.oraclevalidator.Msg", "AggregateValidatorPrevote", data);
        return promise.then((data) => MsgAggregateValidatorPrevoteResponse.decode(new Reader(data)));
    }
    AggregateValidatorVote(request) {
        const data = MsgAggregateValidatorVote.encode(request).finish();
        const promise = this.rpc.request("edx04.oraclevalidator.oraclevalidator.Msg", "AggregateValidatorVote", data);
        return promise.then((data) => MsgAggregateValidatorVoteResponse.decode(new Reader(data)));
    }
    DelegateFeedConsent(request) {
        const data = MsgDelegateFeedConsent.encode(request).finish();
        const promise = this.rpc.request("edx04.oraclevalidator.oraclevalidator.Msg", "DelegateFeedConsent", data);
        return promise.then((data) => MsgDelegateFeedConsentResponse.decode(new Reader(data)));
    }
}
