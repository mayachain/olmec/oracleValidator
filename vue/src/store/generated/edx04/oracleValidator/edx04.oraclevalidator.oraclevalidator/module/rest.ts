/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

export interface OraclevalidatorAggregateValidatorPrevote {
  hash?: string;
  voter?: string;

  /** @format uint64 */
  submitBlock?: string;
}

export interface OraclevalidatorAggregateValidatorVote {
  validators?: OraclevalidatorValidator[];
  voter?: string;
}

/**
 * MsgAggregateValidatorPrevoteResponse defines the Msg/AggregateValidatorPrevote response type.
 */
export type OraclevalidatorMsgAggregateValidatorPrevoteResponse = object;

/**
 * MsgAggregateValidatorVoteResponse defines the Msg/AggregateExchangeRateVote response type.
 */
export type OraclevalidatorMsgAggregateValidatorVoteResponse = object;

/**
 * MsgDelegateFeedConsentResponse defines the Msg/DelegateFeedConsent response type.
 */
export type OraclevalidatorMsgDelegateFeedConsentResponse = object;

/**
 * Params defines the parameters for the module.
 */
export interface OraclevalidatorParams {
  /** @format uint64 */
  votePeriod?: string;
  voteThreshold?: string;
  whitelist?: OraclevalidatorValidator[];
}

/**
* QueryActivesResponse is response type for the
Query/Actives RPC method.
*/
export interface OraclevalidatorQueryActivesResponse {
  /** actives defines a list of the validators which oracle aggreed to whitelist. */
  actives?: string[];
}

/**
* QueryAggregatePrevoteResponse is response type for the
Query/AggregatePrevote RPC method.
*/
export interface OraclevalidatorQueryAggregatePrevoteResponse {
  aggregatePrevote?: OraclevalidatorAggregateValidatorPrevote;
}

/**
* QueryAggregatePrevotesResponse is response type for the
Query/AggregatePrevotes RPC method.
*/
export interface OraclevalidatorQueryAggregatePrevotesResponse {
  aggregatePrevotes?: OraclevalidatorAggregateValidatorPrevote[];
}

/**
* QueryAggregateVoteResponse is response type for the
Query/AggregateVote RPC method.
*/
export interface OraclevalidatorQueryAggregateVoteResponse {
  aggregateVote?: OraclevalidatorAggregateValidatorVote;
}

/**
* QueryAggregateVotesResponse is response type for the
Query/AggregateVotes RPC method.
*/
export interface OraclevalidatorQueryAggregateVotesResponse {
  aggregateVotes?: OraclevalidatorAggregateValidatorVote[];
}

/**
* QueryFeederDelegationResponse is response type for the
Query/FeederDelegation RPC method.
*/
export interface OraclevalidatorQueryFeederDelegationResponse {
  feederAddr?: string;
}

/**
 * QueryParamsResponse is response type for the Query/Params RPC method.
 */
export interface OraclevalidatorQueryParamsResponse {
  /** params holds all the parameters of this module. */
  params?: OraclevalidatorParams;
}

export interface OraclevalidatorValidator {
  address?: string;
}

export interface ProtobufAny {
  "@type"?: string;
}

export interface RpcStatus {
  /** @format int32 */
  code?: number;
  message?: string;
  details?: ProtobufAny[];
}

export type QueryParamsType = Record<string | number, any>;
export type ResponseFormat = keyof Omit<Body, "body" | "bodyUsed">;

export interface FullRequestParams extends Omit<RequestInit, "body"> {
  /** set parameter to `true` for call `securityWorker` for this request */
  secure?: boolean;
  /** request path */
  path: string;
  /** content type of request body */
  type?: ContentType;
  /** query params */
  query?: QueryParamsType;
  /** format of response (i.e. response.json() -> format: "json") */
  format?: keyof Omit<Body, "body" | "bodyUsed">;
  /** request body */
  body?: unknown;
  /** base url */
  baseUrl?: string;
  /** request cancellation token */
  cancelToken?: CancelToken;
}

export type RequestParams = Omit<FullRequestParams, "body" | "method" | "query" | "path">;

export interface ApiConfig<SecurityDataType = unknown> {
  baseUrl?: string;
  baseApiParams?: Omit<RequestParams, "baseUrl" | "cancelToken" | "signal">;
  securityWorker?: (securityData: SecurityDataType) => RequestParams | void;
}

export interface HttpResponse<D extends unknown, E extends unknown = unknown> extends Response {
  data: D;
  error: E;
}

type CancelToken = Symbol | string | number;

export enum ContentType {
  Json = "application/json",
  FormData = "multipart/form-data",
  UrlEncoded = "application/x-www-form-urlencoded",
}

export class HttpClient<SecurityDataType = unknown> {
  public baseUrl: string = "";
  private securityData: SecurityDataType = null as any;
  private securityWorker: null | ApiConfig<SecurityDataType>["securityWorker"] = null;
  private abortControllers = new Map<CancelToken, AbortController>();

  private baseApiParams: RequestParams = {
    credentials: "same-origin",
    headers: {},
    redirect: "follow",
    referrerPolicy: "no-referrer",
  };

  constructor(apiConfig: ApiConfig<SecurityDataType> = {}) {
    Object.assign(this, apiConfig);
  }

  public setSecurityData = (data: SecurityDataType) => {
    this.securityData = data;
  };

  private addQueryParam(query: QueryParamsType, key: string) {
    const value = query[key];

    return (
      encodeURIComponent(key) +
      "=" +
      encodeURIComponent(Array.isArray(value) ? value.join(",") : typeof value === "number" ? value : `${value}`)
    );
  }

  protected toQueryString(rawQuery?: QueryParamsType): string {
    const query = rawQuery || {};
    const keys = Object.keys(query).filter((key) => "undefined" !== typeof query[key]);
    return keys
      .map((key) =>
        typeof query[key] === "object" && !Array.isArray(query[key])
          ? this.toQueryString(query[key] as QueryParamsType)
          : this.addQueryParam(query, key),
      )
      .join("&");
  }

  protected addQueryParams(rawQuery?: QueryParamsType): string {
    const queryString = this.toQueryString(rawQuery);
    return queryString ? `?${queryString}` : "";
  }

  private contentFormatters: Record<ContentType, (input: any) => any> = {
    [ContentType.Json]: (input: any) =>
      input !== null && (typeof input === "object" || typeof input === "string") ? JSON.stringify(input) : input,
    [ContentType.FormData]: (input: any) =>
      Object.keys(input || {}).reduce((data, key) => {
        data.append(key, input[key]);
        return data;
      }, new FormData()),
    [ContentType.UrlEncoded]: (input: any) => this.toQueryString(input),
  };

  private mergeRequestParams(params1: RequestParams, params2?: RequestParams): RequestParams {
    return {
      ...this.baseApiParams,
      ...params1,
      ...(params2 || {}),
      headers: {
        ...(this.baseApiParams.headers || {}),
        ...(params1.headers || {}),
        ...((params2 && params2.headers) || {}),
      },
    };
  }

  private createAbortSignal = (cancelToken: CancelToken): AbortSignal | undefined => {
    if (this.abortControllers.has(cancelToken)) {
      const abortController = this.abortControllers.get(cancelToken);
      if (abortController) {
        return abortController.signal;
      }
      return void 0;
    }

    const abortController = new AbortController();
    this.abortControllers.set(cancelToken, abortController);
    return abortController.signal;
  };

  public abortRequest = (cancelToken: CancelToken) => {
    const abortController = this.abortControllers.get(cancelToken);

    if (abortController) {
      abortController.abort();
      this.abortControllers.delete(cancelToken);
    }
  };

  public request = <T = any, E = any>({
    body,
    secure,
    path,
    type,
    query,
    format = "json",
    baseUrl,
    cancelToken,
    ...params
  }: FullRequestParams): Promise<HttpResponse<T, E>> => {
    const secureParams = (secure && this.securityWorker && this.securityWorker(this.securityData)) || {};
    const requestParams = this.mergeRequestParams(params, secureParams);
    const queryString = query && this.toQueryString(query);
    const payloadFormatter = this.contentFormatters[type || ContentType.Json];

    return fetch(`${baseUrl || this.baseUrl || ""}${path}${queryString ? `?${queryString}` : ""}`, {
      ...requestParams,
      headers: {
        ...(type && type !== ContentType.FormData ? { "Content-Type": type } : {}),
        ...(requestParams.headers || {}),
      },
      signal: cancelToken ? this.createAbortSignal(cancelToken) : void 0,
      body: typeof body === "undefined" || body === null ? null : payloadFormatter(body),
    }).then(async (response) => {
      const r = response as HttpResponse<T, E>;
      r.data = (null as unknown) as T;
      r.error = (null as unknown) as E;

      const data = await response[format]()
        .then((data) => {
          if (r.ok) {
            r.data = data;
          } else {
            r.error = data;
          }
          return r;
        })
        .catch((e) => {
          r.error = e;
          return r;
        });

      if (cancelToken) {
        this.abortControllers.delete(cancelToken);
      }

      if (!response.ok) throw data;
      return data;
    });
  };
}

/**
 * @title oraclevalidator/genesis.proto
 * @version version not set
 */
export class Api<SecurityDataType extends unknown> extends HttpClient<SecurityDataType> {
  /**
   * No description
   *
   * @tags Query
   * @name QueryParams
   * @summary Parameters queries the parameters of the module.
   * @request GET:/edx04/oraclevalidator/oraclevalidator/params
   */
  queryParams = (params: RequestParams = {}) =>
    this.request<OraclevalidatorQueryParamsResponse, RpcStatus>({
      path: `/edx04/oraclevalidator/oraclevalidator/params`,
      method: "GET",
      format: "json",
      ...params,
    });

  /**
   * No description
   *
   * @tags Query
   * @name QueryActives
   * @summary Actives returns all whitelisted validators
   * @request GET:/edx04/oraclevalidator/oraclevalidator/validators
   */
  queryActives = (params: RequestParams = {}) =>
    this.request<OraclevalidatorQueryActivesResponse, RpcStatus>({
      path: `/edx04/oraclevalidator/oraclevalidator/validators`,
      method: "GET",
      format: "json",
      ...params,
    });

  /**
   * No description
   *
   * @tags Query
   * @name QueryFeederDelegation
   * @summary FeederDelegation returns feeder delegation of a validator
   * @request GET:/edx04/oraclevalidator/oraclevalidator/validators/{validatorAddr}/feeder
   */
  queryFeederDelegation = (validatorAddr: string, params: RequestParams = {}) =>
    this.request<OraclevalidatorQueryFeederDelegationResponse, RpcStatus>({
      path: `/edx04/oraclevalidator/oraclevalidator/validators/${validatorAddr}/feeder`,
      method: "GET",
      format: "json",
      ...params,
    });

  /**
   * No description
   *
   * @tags Query
   * @name QueryAggregateVote
   * @summary AggregateVote returns an aggregate vote of a validator
   * @request GET:/edx04/oraclevalidator/valdiators/{validatorAddr}/aggregate_vote
   */
  queryAggregateVote = (validatorAddr: string, params: RequestParams = {}) =>
    this.request<OraclevalidatorQueryAggregateVoteResponse, RpcStatus>({
      path: `/edx04/oraclevalidator/valdiators/${validatorAddr}/aggregate_vote`,
      method: "GET",
      format: "json",
      ...params,
    });

  /**
   * No description
   *
   * @tags Query
   * @name QueryAggregatePrevotes
   * @summary AggregatePrevotes returns aggregate prevotes of all validators
   * @request GET:/edx04/oraclevalidator/validators/aggregate_prevotes
   */
  queryAggregatePrevotes = (params: RequestParams = {}) =>
    this.request<OraclevalidatorQueryAggregatePrevotesResponse, RpcStatus>({
      path: `/edx04/oraclevalidator/validators/aggregate_prevotes`,
      method: "GET",
      format: "json",
      ...params,
    });

  /**
   * No description
   *
   * @tags Query
   * @name QueryAggregateVotes
   * @summary AggregateVotes returns aggregate votes of all validators
   * @request GET:/edx04/oraclevalidator/validators/aggregate_votes
   */
  queryAggregateVotes = (params: RequestParams = {}) =>
    this.request<OraclevalidatorQueryAggregateVotesResponse, RpcStatus>({
      path: `/edx04/oraclevalidator/validators/aggregate_votes`,
      method: "GET",
      format: "json",
      ...params,
    });

  /**
   * No description
   *
   * @tags Query
   * @name QueryAggregatePrevote
   * @summary AggregatePrevote returns an aggregate prevote of a validator
   * @request GET:/edx04/oraclevalidator/validators/{validatorAddr}/aggregate_prevote
   */
  queryAggregatePrevote = (validatorAddr: string, params: RequestParams = {}) =>
    this.request<OraclevalidatorQueryAggregatePrevoteResponse, RpcStatus>({
      path: `/edx04/oraclevalidator/validators/${validatorAddr}/aggregate_prevote`,
      method: "GET",
      format: "json",
      ...params,
    });
}
