/* eslint-disable */
import { Reader, Writer } from "protobufjs/minimal";
import { Params, AggregateValidatorPrevote, AggregateValidatorVote, } from "../oraclevalidator/params";
export const protobufPackage = "edx04.oraclevalidator.oraclevalidator";
const baseQueryParamsRequest = {};
export const QueryParamsRequest = {
    encode(_, writer = Writer.create()) {
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = { ...baseQueryParamsRequest };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(_) {
        const message = { ...baseQueryParamsRequest };
        return message;
    },
    toJSON(_) {
        const obj = {};
        return obj;
    },
    fromPartial(_) {
        const message = { ...baseQueryParamsRequest };
        return message;
    },
};
const baseQueryParamsResponse = {};
export const QueryParamsResponse = {
    encode(message, writer = Writer.create()) {
        if (message.params !== undefined) {
            Params.encode(message.params, writer.uint32(10).fork()).ldelim();
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = { ...baseQueryParamsResponse };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.params = Params.decode(reader, reader.uint32());
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = { ...baseQueryParamsResponse };
        if (object.params !== undefined && object.params !== null) {
            message.params = Params.fromJSON(object.params);
        }
        else {
            message.params = undefined;
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.params !== undefined &&
            (obj.params = message.params ? Params.toJSON(message.params) : undefined);
        return obj;
    },
    fromPartial(object) {
        const message = { ...baseQueryParamsResponse };
        if (object.params !== undefined && object.params !== null) {
            message.params = Params.fromPartial(object.params);
        }
        else {
            message.params = undefined;
        }
        return message;
    },
};
const baseQueryActivesRequest = {};
export const QueryActivesRequest = {
    encode(_, writer = Writer.create()) {
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = { ...baseQueryActivesRequest };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(_) {
        const message = { ...baseQueryActivesRequest };
        return message;
    },
    toJSON(_) {
        const obj = {};
        return obj;
    },
    fromPartial(_) {
        const message = { ...baseQueryActivesRequest };
        return message;
    },
};
const baseQueryActivesResponse = { actives: "" };
export const QueryActivesResponse = {
    encode(message, writer = Writer.create()) {
        for (const v of message.actives) {
            writer.uint32(10).string(v);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = { ...baseQueryActivesResponse };
        message.actives = [];
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.actives.push(reader.string());
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = { ...baseQueryActivesResponse };
        message.actives = [];
        if (object.actives !== undefined && object.actives !== null) {
            for (const e of object.actives) {
                message.actives.push(String(e));
            }
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        if (message.actives) {
            obj.actives = message.actives.map((e) => e);
        }
        else {
            obj.actives = [];
        }
        return obj;
    },
    fromPartial(object) {
        const message = { ...baseQueryActivesResponse };
        message.actives = [];
        if (object.actives !== undefined && object.actives !== null) {
            for (const e of object.actives) {
                message.actives.push(e);
            }
        }
        return message;
    },
};
const baseQueryFeederDelegationRequest = { validatorAddr: "" };
export const QueryFeederDelegationRequest = {
    encode(message, writer = Writer.create()) {
        if (message.validatorAddr !== "") {
            writer.uint32(10).string(message.validatorAddr);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = {
            ...baseQueryFeederDelegationRequest,
        };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.validatorAddr = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = {
            ...baseQueryFeederDelegationRequest,
        };
        if (object.validatorAddr !== undefined && object.validatorAddr !== null) {
            message.validatorAddr = String(object.validatorAddr);
        }
        else {
            message.validatorAddr = "";
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.validatorAddr !== undefined &&
            (obj.validatorAddr = message.validatorAddr);
        return obj;
    },
    fromPartial(object) {
        const message = {
            ...baseQueryFeederDelegationRequest,
        };
        if (object.validatorAddr !== undefined && object.validatorAddr !== null) {
            message.validatorAddr = object.validatorAddr;
        }
        else {
            message.validatorAddr = "";
        }
        return message;
    },
};
const baseQueryFeederDelegationResponse = { feederAddr: "" };
export const QueryFeederDelegationResponse = {
    encode(message, writer = Writer.create()) {
        if (message.feederAddr !== "") {
            writer.uint32(10).string(message.feederAddr);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = {
            ...baseQueryFeederDelegationResponse,
        };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.feederAddr = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = {
            ...baseQueryFeederDelegationResponse,
        };
        if (object.feederAddr !== undefined && object.feederAddr !== null) {
            message.feederAddr = String(object.feederAddr);
        }
        else {
            message.feederAddr = "";
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.feederAddr !== undefined && (obj.feederAddr = message.feederAddr);
        return obj;
    },
    fromPartial(object) {
        const message = {
            ...baseQueryFeederDelegationResponse,
        };
        if (object.feederAddr !== undefined && object.feederAddr !== null) {
            message.feederAddr = object.feederAddr;
        }
        else {
            message.feederAddr = "";
        }
        return message;
    },
};
const baseQueryAggregatePrevoteRequest = { validatorAddr: "" };
export const QueryAggregatePrevoteRequest = {
    encode(message, writer = Writer.create()) {
        if (message.validatorAddr !== "") {
            writer.uint32(10).string(message.validatorAddr);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = {
            ...baseQueryAggregatePrevoteRequest,
        };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.validatorAddr = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = {
            ...baseQueryAggregatePrevoteRequest,
        };
        if (object.validatorAddr !== undefined && object.validatorAddr !== null) {
            message.validatorAddr = String(object.validatorAddr);
        }
        else {
            message.validatorAddr = "";
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.validatorAddr !== undefined &&
            (obj.validatorAddr = message.validatorAddr);
        return obj;
    },
    fromPartial(object) {
        const message = {
            ...baseQueryAggregatePrevoteRequest,
        };
        if (object.validatorAddr !== undefined && object.validatorAddr !== null) {
            message.validatorAddr = object.validatorAddr;
        }
        else {
            message.validatorAddr = "";
        }
        return message;
    },
};
const baseQueryAggregatePrevoteResponse = {};
export const QueryAggregatePrevoteResponse = {
    encode(message, writer = Writer.create()) {
        if (message.aggregatePrevote !== undefined) {
            AggregateValidatorPrevote.encode(message.aggregatePrevote, writer.uint32(10).fork()).ldelim();
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = {
            ...baseQueryAggregatePrevoteResponse,
        };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.aggregatePrevote = AggregateValidatorPrevote.decode(reader, reader.uint32());
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = {
            ...baseQueryAggregatePrevoteResponse,
        };
        if (object.aggregatePrevote !== undefined &&
            object.aggregatePrevote !== null) {
            message.aggregatePrevote = AggregateValidatorPrevote.fromJSON(object.aggregatePrevote);
        }
        else {
            message.aggregatePrevote = undefined;
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.aggregatePrevote !== undefined &&
            (obj.aggregatePrevote = message.aggregatePrevote
                ? AggregateValidatorPrevote.toJSON(message.aggregatePrevote)
                : undefined);
        return obj;
    },
    fromPartial(object) {
        const message = {
            ...baseQueryAggregatePrevoteResponse,
        };
        if (object.aggregatePrevote !== undefined &&
            object.aggregatePrevote !== null) {
            message.aggregatePrevote = AggregateValidatorPrevote.fromPartial(object.aggregatePrevote);
        }
        else {
            message.aggregatePrevote = undefined;
        }
        return message;
    },
};
const baseQueryAggregatePrevotesRequest = {};
export const QueryAggregatePrevotesRequest = {
    encode(_, writer = Writer.create()) {
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = {
            ...baseQueryAggregatePrevotesRequest,
        };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(_) {
        const message = {
            ...baseQueryAggregatePrevotesRequest,
        };
        return message;
    },
    toJSON(_) {
        const obj = {};
        return obj;
    },
    fromPartial(_) {
        const message = {
            ...baseQueryAggregatePrevotesRequest,
        };
        return message;
    },
};
const baseQueryAggregatePrevotesResponse = {};
export const QueryAggregatePrevotesResponse = {
    encode(message, writer = Writer.create()) {
        for (const v of message.aggregatePrevotes) {
            AggregateValidatorPrevote.encode(v, writer.uint32(10).fork()).ldelim();
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = {
            ...baseQueryAggregatePrevotesResponse,
        };
        message.aggregatePrevotes = [];
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.aggregatePrevotes.push(AggregateValidatorPrevote.decode(reader, reader.uint32()));
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = {
            ...baseQueryAggregatePrevotesResponse,
        };
        message.aggregatePrevotes = [];
        if (object.aggregatePrevotes !== undefined &&
            object.aggregatePrevotes !== null) {
            for (const e of object.aggregatePrevotes) {
                message.aggregatePrevotes.push(AggregateValidatorPrevote.fromJSON(e));
            }
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        if (message.aggregatePrevotes) {
            obj.aggregatePrevotes = message.aggregatePrevotes.map((e) => e ? AggregateValidatorPrevote.toJSON(e) : undefined);
        }
        else {
            obj.aggregatePrevotes = [];
        }
        return obj;
    },
    fromPartial(object) {
        const message = {
            ...baseQueryAggregatePrevotesResponse,
        };
        message.aggregatePrevotes = [];
        if (object.aggregatePrevotes !== undefined &&
            object.aggregatePrevotes !== null) {
            for (const e of object.aggregatePrevotes) {
                message.aggregatePrevotes.push(AggregateValidatorPrevote.fromPartial(e));
            }
        }
        return message;
    },
};
const baseQueryAggregateVoteRequest = { validatorAddr: "" };
export const QueryAggregateVoteRequest = {
    encode(message, writer = Writer.create()) {
        if (message.validatorAddr !== "") {
            writer.uint32(10).string(message.validatorAddr);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = {
            ...baseQueryAggregateVoteRequest,
        };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.validatorAddr = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = {
            ...baseQueryAggregateVoteRequest,
        };
        if (object.validatorAddr !== undefined && object.validatorAddr !== null) {
            message.validatorAddr = String(object.validatorAddr);
        }
        else {
            message.validatorAddr = "";
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.validatorAddr !== undefined &&
            (obj.validatorAddr = message.validatorAddr);
        return obj;
    },
    fromPartial(object) {
        const message = {
            ...baseQueryAggregateVoteRequest,
        };
        if (object.validatorAddr !== undefined && object.validatorAddr !== null) {
            message.validatorAddr = object.validatorAddr;
        }
        else {
            message.validatorAddr = "";
        }
        return message;
    },
};
const baseQueryAggregateVoteResponse = {};
export const QueryAggregateVoteResponse = {
    encode(message, writer = Writer.create()) {
        if (message.aggregateVote !== undefined) {
            AggregateValidatorVote.encode(message.aggregateVote, writer.uint32(10).fork()).ldelim();
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = {
            ...baseQueryAggregateVoteResponse,
        };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.aggregateVote = AggregateValidatorVote.decode(reader, reader.uint32());
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = {
            ...baseQueryAggregateVoteResponse,
        };
        if (object.aggregateVote !== undefined && object.aggregateVote !== null) {
            message.aggregateVote = AggregateValidatorVote.fromJSON(object.aggregateVote);
        }
        else {
            message.aggregateVote = undefined;
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.aggregateVote !== undefined &&
            (obj.aggregateVote = message.aggregateVote
                ? AggregateValidatorVote.toJSON(message.aggregateVote)
                : undefined);
        return obj;
    },
    fromPartial(object) {
        const message = {
            ...baseQueryAggregateVoteResponse,
        };
        if (object.aggregateVote !== undefined && object.aggregateVote !== null) {
            message.aggregateVote = AggregateValidatorVote.fromPartial(object.aggregateVote);
        }
        else {
            message.aggregateVote = undefined;
        }
        return message;
    },
};
const baseQueryAggregateVotesRequest = {};
export const QueryAggregateVotesRequest = {
    encode(_, writer = Writer.create()) {
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = {
            ...baseQueryAggregateVotesRequest,
        };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(_) {
        const message = {
            ...baseQueryAggregateVotesRequest,
        };
        return message;
    },
    toJSON(_) {
        const obj = {};
        return obj;
    },
    fromPartial(_) {
        const message = {
            ...baseQueryAggregateVotesRequest,
        };
        return message;
    },
};
const baseQueryAggregateVotesResponse = {};
export const QueryAggregateVotesResponse = {
    encode(message, writer = Writer.create()) {
        for (const v of message.aggregateVotes) {
            AggregateValidatorVote.encode(v, writer.uint32(10).fork()).ldelim();
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = {
            ...baseQueryAggregateVotesResponse,
        };
        message.aggregateVotes = [];
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.aggregateVotes.push(AggregateValidatorVote.decode(reader, reader.uint32()));
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = {
            ...baseQueryAggregateVotesResponse,
        };
        message.aggregateVotes = [];
        if (object.aggregateVotes !== undefined && object.aggregateVotes !== null) {
            for (const e of object.aggregateVotes) {
                message.aggregateVotes.push(AggregateValidatorVote.fromJSON(e));
            }
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        if (message.aggregateVotes) {
            obj.aggregateVotes = message.aggregateVotes.map((e) => e ? AggregateValidatorVote.toJSON(e) : undefined);
        }
        else {
            obj.aggregateVotes = [];
        }
        return obj;
    },
    fromPartial(object) {
        const message = {
            ...baseQueryAggregateVotesResponse,
        };
        message.aggregateVotes = [];
        if (object.aggregateVotes !== undefined && object.aggregateVotes !== null) {
            for (const e of object.aggregateVotes) {
                message.aggregateVotes.push(AggregateValidatorVote.fromPartial(e));
            }
        }
        return message;
    },
};
export class QueryClientImpl {
    constructor(rpc) {
        this.rpc = rpc;
    }
    Params(request) {
        const data = QueryParamsRequest.encode(request).finish();
        const promise = this.rpc.request("edx04.oraclevalidator.oraclevalidator.Query", "Params", data);
        return promise.then((data) => QueryParamsResponse.decode(new Reader(data)));
    }
    Actives(request) {
        const data = QueryActivesRequest.encode(request).finish();
        const promise = this.rpc.request("edx04.oraclevalidator.oraclevalidator.Query", "Actives", data);
        return promise.then((data) => QueryActivesResponse.decode(new Reader(data)));
    }
    FeederDelegation(request) {
        const data = QueryFeederDelegationRequest.encode(request).finish();
        const promise = this.rpc.request("edx04.oraclevalidator.oraclevalidator.Query", "FeederDelegation", data);
        return promise.then((data) => QueryFeederDelegationResponse.decode(new Reader(data)));
    }
    AggregatePrevote(request) {
        const data = QueryAggregatePrevoteRequest.encode(request).finish();
        const promise = this.rpc.request("edx04.oraclevalidator.oraclevalidator.Query", "AggregatePrevote", data);
        return promise.then((data) => QueryAggregatePrevoteResponse.decode(new Reader(data)));
    }
    AggregatePrevotes(request) {
        const data = QueryAggregatePrevotesRequest.encode(request).finish();
        const promise = this.rpc.request("edx04.oraclevalidator.oraclevalidator.Query", "AggregatePrevotes", data);
        return promise.then((data) => QueryAggregatePrevotesResponse.decode(new Reader(data)));
    }
    AggregateVote(request) {
        const data = QueryAggregateVoteRequest.encode(request).finish();
        const promise = this.rpc.request("edx04.oraclevalidator.oraclevalidator.Query", "AggregateVote", data);
        return promise.then((data) => QueryAggregateVoteResponse.decode(new Reader(data)));
    }
    AggregateVotes(request) {
        const data = QueryAggregateVotesRequest.encode(request).finish();
        const promise = this.rpc.request("edx04.oraclevalidator.oraclevalidator.Query", "AggregateVotes", data);
        return promise.then((data) => QueryAggregateVotesResponse.decode(new Reader(data)));
    }
}
