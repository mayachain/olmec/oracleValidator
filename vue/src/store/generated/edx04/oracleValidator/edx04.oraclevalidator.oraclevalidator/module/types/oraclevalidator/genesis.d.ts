import { Params, Validator } from "../oraclevalidator/params";
import { Writer, Reader } from "protobufjs/minimal";
export declare const protobufPackage = "edx04.oraclevalidator.oraclevalidator";
/** GenesisState defines the oraclevalidator module's genesis state. */
export interface GenesisState {
    params: Params | undefined;
    whitelistValidators: Validator[];
    /** this line is used by starport scaffolding # genesis/proto/state */
    feederDelegations: FeederDelegation[];
}
export interface FeederDelegation {
    feederAddress: string;
    validatorAddress: string;
}
export declare const GenesisState: {
    encode(message: GenesisState, writer?: Writer): Writer;
    decode(input: Reader | Uint8Array, length?: number): GenesisState;
    fromJSON(object: any): GenesisState;
    toJSON(message: GenesisState): unknown;
    fromPartial(object: DeepPartial<GenesisState>): GenesisState;
};
export declare const FeederDelegation: {
    encode(message: FeederDelegation, writer?: Writer): Writer;
    decode(input: Reader | Uint8Array, length?: number): FeederDelegation;
    fromJSON(object: any): FeederDelegation;
    toJSON(message: FeederDelegation): unknown;
    fromPartial(object: DeepPartial<FeederDelegation>): FeederDelegation;
};
declare type Builtin = Date | Function | Uint8Array | string | number | undefined;
export declare type DeepPartial<T> = T extends Builtin ? T : T extends Array<infer U> ? Array<DeepPartial<U>> : T extends ReadonlyArray<infer U> ? ReadonlyArray<DeepPartial<U>> : T extends {} ? {
    [K in keyof T]?: DeepPartial<T[K]>;
} : Partial<T>;
export {};
