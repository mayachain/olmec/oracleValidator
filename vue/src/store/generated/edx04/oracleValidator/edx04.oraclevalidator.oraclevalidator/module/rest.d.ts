export interface OraclevalidatorAggregateValidatorPrevote {
    hash?: string;
    voter?: string;
    /** @format uint64 */
    submitBlock?: string;
}
export interface OraclevalidatorAggregateValidatorVote {
    validators?: OraclevalidatorValidator[];
    voter?: string;
}
/**
 * MsgAggregateValidatorPrevoteResponse defines the Msg/AggregateValidatorPrevote response type.
 */
export declare type OraclevalidatorMsgAggregateValidatorPrevoteResponse = object;
/**
 * MsgAggregateValidatorVoteResponse defines the Msg/AggregateExchangeRateVote response type.
 */
export declare type OraclevalidatorMsgAggregateValidatorVoteResponse = object;
/**
 * MsgDelegateFeedConsentResponse defines the Msg/DelegateFeedConsent response type.
 */
export declare type OraclevalidatorMsgDelegateFeedConsentResponse = object;
/**
 * Params defines the parameters for the module.
 */
export interface OraclevalidatorParams {
    /** @format uint64 */
    votePeriod?: string;
    voteThreshold?: string;
    whitelist?: OraclevalidatorValidator[];
}
/**
* QueryActivesResponse is response type for the
Query/Actives RPC method.
*/
export interface OraclevalidatorQueryActivesResponse {
    /** actives defines a list of the validators which oracle aggreed to whitelist. */
    actives?: string[];
}
/**
* QueryAggregatePrevoteResponse is response type for the
Query/AggregatePrevote RPC method.
*/
export interface OraclevalidatorQueryAggregatePrevoteResponse {
    aggregatePrevote?: OraclevalidatorAggregateValidatorPrevote;
}
/**
* QueryAggregatePrevotesResponse is response type for the
Query/AggregatePrevotes RPC method.
*/
export interface OraclevalidatorQueryAggregatePrevotesResponse {
    aggregatePrevotes?: OraclevalidatorAggregateValidatorPrevote[];
}
/**
* QueryAggregateVoteResponse is response type for the
Query/AggregateVote RPC method.
*/
export interface OraclevalidatorQueryAggregateVoteResponse {
    aggregateVote?: OraclevalidatorAggregateValidatorVote;
}
/**
* QueryAggregateVotesResponse is response type for the
Query/AggregateVotes RPC method.
*/
export interface OraclevalidatorQueryAggregateVotesResponse {
    aggregateVotes?: OraclevalidatorAggregateValidatorVote[];
}
/**
* QueryFeederDelegationResponse is response type for the
Query/FeederDelegation RPC method.
*/
export interface OraclevalidatorQueryFeederDelegationResponse {
    feederAddr?: string;
}
/**
 * QueryParamsResponse is response type for the Query/Params RPC method.
 */
export interface OraclevalidatorQueryParamsResponse {
    /** params holds all the parameters of this module. */
    params?: OraclevalidatorParams;
}
export interface OraclevalidatorValidator {
    address?: string;
}
export interface ProtobufAny {
    "@type"?: string;
}
export interface RpcStatus {
    /** @format int32 */
    code?: number;
    message?: string;
    details?: ProtobufAny[];
}
export declare type QueryParamsType = Record<string | number, any>;
export declare type ResponseFormat = keyof Omit<Body, "body" | "bodyUsed">;
export interface FullRequestParams extends Omit<RequestInit, "body"> {
    /** set parameter to `true` for call `securityWorker` for this request */
    secure?: boolean;
    /** request path */
    path: string;
    /** content type of request body */
    type?: ContentType;
    /** query params */
    query?: QueryParamsType;
    /** format of response (i.e. response.json() -> format: "json") */
    format?: keyof Omit<Body, "body" | "bodyUsed">;
    /** request body */
    body?: unknown;
    /** base url */
    baseUrl?: string;
    /** request cancellation token */
    cancelToken?: CancelToken;
}
export declare type RequestParams = Omit<FullRequestParams, "body" | "method" | "query" | "path">;
export interface ApiConfig<SecurityDataType = unknown> {
    baseUrl?: string;
    baseApiParams?: Omit<RequestParams, "baseUrl" | "cancelToken" | "signal">;
    securityWorker?: (securityData: SecurityDataType) => RequestParams | void;
}
export interface HttpResponse<D extends unknown, E extends unknown = unknown> extends Response {
    data: D;
    error: E;
}
declare type CancelToken = Symbol | string | number;
export declare enum ContentType {
    Json = "application/json",
    FormData = "multipart/form-data",
    UrlEncoded = "application/x-www-form-urlencoded"
}
export declare class HttpClient<SecurityDataType = unknown> {
    baseUrl: string;
    private securityData;
    private securityWorker;
    private abortControllers;
    private baseApiParams;
    constructor(apiConfig?: ApiConfig<SecurityDataType>);
    setSecurityData: (data: SecurityDataType) => void;
    private addQueryParam;
    protected toQueryString(rawQuery?: QueryParamsType): string;
    protected addQueryParams(rawQuery?: QueryParamsType): string;
    private contentFormatters;
    private mergeRequestParams;
    private createAbortSignal;
    abortRequest: (cancelToken: CancelToken) => void;
    request: <T = any, E = any>({ body, secure, path, type, query, format, baseUrl, cancelToken, ...params }: FullRequestParams) => Promise<HttpResponse<T, E>>;
}
/**
 * @title oraclevalidator/genesis.proto
 * @version version not set
 */
export declare class Api<SecurityDataType extends unknown> extends HttpClient<SecurityDataType> {
    /**
     * No description
     *
     * @tags Query
     * @name QueryParams
     * @summary Parameters queries the parameters of the module.
     * @request GET:/edx04/oraclevalidator/oraclevalidator/params
     */
    queryParams: (params?: RequestParams) => Promise<HttpResponse<OraclevalidatorQueryParamsResponse, RpcStatus>>;
    /**
     * No description
     *
     * @tags Query
     * @name QueryActives
     * @summary Actives returns all whitelisted validators
     * @request GET:/edx04/oraclevalidator/oraclevalidator/validators
     */
    queryActives: (params?: RequestParams) => Promise<HttpResponse<OraclevalidatorQueryActivesResponse, RpcStatus>>;
    /**
     * No description
     *
     * @tags Query
     * @name QueryFeederDelegation
     * @summary FeederDelegation returns feeder delegation of a validator
     * @request GET:/edx04/oraclevalidator/oraclevalidator/validators/{validatorAddr}/feeder
     */
    queryFeederDelegation: (validatorAddr: string, params?: RequestParams) => Promise<HttpResponse<OraclevalidatorQueryFeederDelegationResponse, RpcStatus>>;
    /**
     * No description
     *
     * @tags Query
     * @name QueryAggregateVote
     * @summary AggregateVote returns an aggregate vote of a validator
     * @request GET:/edx04/oraclevalidator/valdiators/{validatorAddr}/aggregate_vote
     */
    queryAggregateVote: (validatorAddr: string, params?: RequestParams) => Promise<HttpResponse<OraclevalidatorQueryAggregateVoteResponse, RpcStatus>>;
    /**
     * No description
     *
     * @tags Query
     * @name QueryAggregatePrevotes
     * @summary AggregatePrevotes returns aggregate prevotes of all validators
     * @request GET:/edx04/oraclevalidator/validators/aggregate_prevotes
     */
    queryAggregatePrevotes: (params?: RequestParams) => Promise<HttpResponse<OraclevalidatorQueryAggregatePrevotesResponse, RpcStatus>>;
    /**
     * No description
     *
     * @tags Query
     * @name QueryAggregateVotes
     * @summary AggregateVotes returns aggregate votes of all validators
     * @request GET:/edx04/oraclevalidator/validators/aggregate_votes
     */
    queryAggregateVotes: (params?: RequestParams) => Promise<HttpResponse<OraclevalidatorQueryAggregateVotesResponse, RpcStatus>>;
    /**
     * No description
     *
     * @tags Query
     * @name QueryAggregatePrevote
     * @summary AggregatePrevote returns an aggregate prevote of a validator
     * @request GET:/edx04/oraclevalidator/validators/{validatorAddr}/aggregate_prevote
     */
    queryAggregatePrevote: (validatorAddr: string, params?: RequestParams) => Promise<HttpResponse<OraclevalidatorQueryAggregatePrevoteResponse, RpcStatus>>;
}
export {};
