import { txClient, queryClient, MissingWalletError, registry } from './module';
// @ts-ignore
import { SpVuexError } from '@starport/vuex';
import { FeederDelegation } from "./module/types/oraclevalidator/genesis";
import { Params } from "./module/types/oraclevalidator/params";
import { Validator } from "./module/types/oraclevalidator/params";
import { AggregateValidatorPrevote } from "./module/types/oraclevalidator/params";
import { AggregateValidatorVote } from "./module/types/oraclevalidator/params";
export { FeederDelegation, Params, Validator, AggregateValidatorPrevote, AggregateValidatorVote };
async function initTxClient(vuexGetters) {
    return await txClient(vuexGetters['common/wallet/signer'], {
        addr: vuexGetters['common/env/apiTendermint']
    });
}
async function initQueryClient(vuexGetters) {
    return await queryClient({
        addr: vuexGetters['common/env/apiCosmos']
    });
}
function mergeResults(value, next_values) {
    for (let prop of Object.keys(next_values)) {
        if (Array.isArray(next_values[prop])) {
            value[prop] = [...value[prop], ...next_values[prop]];
        }
        else {
            value[prop] = next_values[prop];
        }
    }
    return value;
}
function getStructure(template) {
    let structure = { fields: [] };
    for (const [key, value] of Object.entries(template)) {
        let field = {};
        field.name = key;
        field.type = typeof value;
        structure.fields.push(field);
    }
    return structure;
}
const getDefaultState = () => {
    return {
        Params: {},
        Actives: {},
        FeederDelegation: {},
        AggregatePrevote: {},
        AggregatePrevotes: {},
        AggregateVote: {},
        AggregateVotes: {},
        _Structure: {
            FeederDelegation: getStructure(FeederDelegation.fromPartial({})),
            Params: getStructure(Params.fromPartial({})),
            Validator: getStructure(Validator.fromPartial({})),
            AggregateValidatorPrevote: getStructure(AggregateValidatorPrevote.fromPartial({})),
            AggregateValidatorVote: getStructure(AggregateValidatorVote.fromPartial({})),
        },
        _Registry: registry,
        _Subscriptions: new Set(),
    };
};
// initial state
const state = getDefaultState();
export default {
    namespaced: true,
    state,
    mutations: {
        RESET_STATE(state) {
            Object.assign(state, getDefaultState());
        },
        QUERY(state, { query, key, value }) {
            state[query][JSON.stringify(key)] = value;
        },
        SUBSCRIBE(state, subscription) {
            state._Subscriptions.add(JSON.stringify(subscription));
        },
        UNSUBSCRIBE(state, subscription) {
            state._Subscriptions.delete(JSON.stringify(subscription));
        }
    },
    getters: {
        getParams: (state) => (params = { params: {} }) => {
            if (!params.query) {
                params.query = null;
            }
            return state.Params[JSON.stringify(params)] ?? {};
        },
        getActives: (state) => (params = { params: {} }) => {
            if (!params.query) {
                params.query = null;
            }
            return state.Actives[JSON.stringify(params)] ?? {};
        },
        getFeederDelegation: (state) => (params = { params: {} }) => {
            if (!params.query) {
                params.query = null;
            }
            return state.FeederDelegation[JSON.stringify(params)] ?? {};
        },
        getAggregatePrevote: (state) => (params = { params: {} }) => {
            if (!params.query) {
                params.query = null;
            }
            return state.AggregatePrevote[JSON.stringify(params)] ?? {};
        },
        getAggregatePrevotes: (state) => (params = { params: {} }) => {
            if (!params.query) {
                params.query = null;
            }
            return state.AggregatePrevotes[JSON.stringify(params)] ?? {};
        },
        getAggregateVote: (state) => (params = { params: {} }) => {
            if (!params.query) {
                params.query = null;
            }
            return state.AggregateVote[JSON.stringify(params)] ?? {};
        },
        getAggregateVotes: (state) => (params = { params: {} }) => {
            if (!params.query) {
                params.query = null;
            }
            return state.AggregateVotes[JSON.stringify(params)] ?? {};
        },
        getTypeStructure: (state) => (type) => {
            return state._Structure[type].fields;
        },
        getRegistry: (state) => {
            return state._Registry;
        }
    },
    actions: {
        init({ dispatch, rootGetters }) {
            console.log('Vuex module: edx04.oraclevalidator.oraclevalidator initialized!');
            if (rootGetters['common/env/client']) {
                rootGetters['common/env/client'].on('newblock', () => {
                    dispatch('StoreUpdate');
                });
            }
        },
        resetState({ commit }) {
            commit('RESET_STATE');
        },
        unsubscribe({ commit }, subscription) {
            commit('UNSUBSCRIBE', subscription);
        },
        async StoreUpdate({ state, dispatch }) {
            state._Subscriptions.forEach(async (subscription) => {
                try {
                    const sub = JSON.parse(subscription);
                    await dispatch(sub.action, sub.payload);
                }
                catch (e) {
                    throw new SpVuexError('Subscriptions: ' + e.message);
                }
            });
        },
        async QueryParams({ commit, rootGetters, getters }, { options: { subscribe, all } = { subscribe: false, all: false }, params, query = null }) {
            try {
                const key = params ?? {};
                const queryClient = await initQueryClient(rootGetters);
                let value = (await queryClient.queryParams()).data;
                commit('QUERY', { query: 'Params', key: { params: { ...key }, query }, value });
                if (subscribe)
                    commit('SUBSCRIBE', { action: 'QueryParams', payload: { options: { all }, params: { ...key }, query } });
                return getters['getParams']({ params: { ...key }, query }) ?? {};
            }
            catch (e) {
                throw new SpVuexError('QueryClient:QueryParams', 'API Node Unavailable. Could not perform query: ' + e.message);
            }
        },
        async QueryActives({ commit, rootGetters, getters }, { options: { subscribe, all } = { subscribe: false, all: false }, params, query = null }) {
            try {
                const key = params ?? {};
                const queryClient = await initQueryClient(rootGetters);
                let value = (await queryClient.queryActives()).data;
                commit('QUERY', { query: 'Actives', key: { params: { ...key }, query }, value });
                if (subscribe)
                    commit('SUBSCRIBE', { action: 'QueryActives', payload: { options: { all }, params: { ...key }, query } });
                return getters['getActives']({ params: { ...key }, query }) ?? {};
            }
            catch (e) {
                throw new SpVuexError('QueryClient:QueryActives', 'API Node Unavailable. Could not perform query: ' + e.message);
            }
        },
        async QueryFeederDelegation({ commit, rootGetters, getters }, { options: { subscribe, all } = { subscribe: false, all: false }, params, query = null }) {
            try {
                const key = params ?? {};
                const queryClient = await initQueryClient(rootGetters);
                let value = (await queryClient.queryFeederDelegation(key.validator_addr)).data;
                commit('QUERY', { query: 'FeederDelegation', key: { params: { ...key }, query }, value });
                if (subscribe)
                    commit('SUBSCRIBE', { action: 'QueryFeederDelegation', payload: { options: { all }, params: { ...key }, query } });
                return getters['getFeederDelegation']({ params: { ...key }, query }) ?? {};
            }
            catch (e) {
                throw new SpVuexError('QueryClient:QueryFeederDelegation', 'API Node Unavailable. Could not perform query: ' + e.message);
            }
        },
        async QueryAggregatePrevote({ commit, rootGetters, getters }, { options: { subscribe, all } = { subscribe: false, all: false }, params, query = null }) {
            try {
                const key = params ?? {};
                const queryClient = await initQueryClient(rootGetters);
                let value = (await queryClient.queryAggregatePrevote(key.validator_addr)).data;
                commit('QUERY', { query: 'AggregatePrevote', key: { params: { ...key }, query }, value });
                if (subscribe)
                    commit('SUBSCRIBE', { action: 'QueryAggregatePrevote', payload: { options: { all }, params: { ...key }, query } });
                return getters['getAggregatePrevote']({ params: { ...key }, query }) ?? {};
            }
            catch (e) {
                throw new SpVuexError('QueryClient:QueryAggregatePrevote', 'API Node Unavailable. Could not perform query: ' + e.message);
            }
        },
        async QueryAggregatePrevotes({ commit, rootGetters, getters }, { options: { subscribe, all } = { subscribe: false, all: false }, params, query = null }) {
            try {
                const key = params ?? {};
                const queryClient = await initQueryClient(rootGetters);
                let value = (await queryClient.queryAggregatePrevotes()).data;
                commit('QUERY', { query: 'AggregatePrevotes', key: { params: { ...key }, query }, value });
                if (subscribe)
                    commit('SUBSCRIBE', { action: 'QueryAggregatePrevotes', payload: { options: { all }, params: { ...key }, query } });
                return getters['getAggregatePrevotes']({ params: { ...key }, query }) ?? {};
            }
            catch (e) {
                throw new SpVuexError('QueryClient:QueryAggregatePrevotes', 'API Node Unavailable. Could not perform query: ' + e.message);
            }
        },
        async QueryAggregateVote({ commit, rootGetters, getters }, { options: { subscribe, all } = { subscribe: false, all: false }, params, query = null }) {
            try {
                const key = params ?? {};
                const queryClient = await initQueryClient(rootGetters);
                let value = (await queryClient.queryAggregateVote(key.validator_addr)).data;
                commit('QUERY', { query: 'AggregateVote', key: { params: { ...key }, query }, value });
                if (subscribe)
                    commit('SUBSCRIBE', { action: 'QueryAggregateVote', payload: { options: { all }, params: { ...key }, query } });
                return getters['getAggregateVote']({ params: { ...key }, query }) ?? {};
            }
            catch (e) {
                throw new SpVuexError('QueryClient:QueryAggregateVote', 'API Node Unavailable. Could not perform query: ' + e.message);
            }
        },
        async QueryAggregateVotes({ commit, rootGetters, getters }, { options: { subscribe, all } = { subscribe: false, all: false }, params, query = null }) {
            try {
                const key = params ?? {};
                const queryClient = await initQueryClient(rootGetters);
                let value = (await queryClient.queryAggregateVotes()).data;
                commit('QUERY', { query: 'AggregateVotes', key: { params: { ...key }, query }, value });
                if (subscribe)
                    commit('SUBSCRIBE', { action: 'QueryAggregateVotes', payload: { options: { all }, params: { ...key }, query } });
                return getters['getAggregateVotes']({ params: { ...key }, query }) ?? {};
            }
            catch (e) {
                throw new SpVuexError('QueryClient:QueryAggregateVotes', 'API Node Unavailable. Could not perform query: ' + e.message);
            }
        },
        async sendMsgAggregateValidatorVote({ rootGetters }, { value, fee = [], memo = '' }) {
            try {
                const txClient = await initTxClient(rootGetters);
                const msg = await txClient.msgAggregateValidatorVote(value);
                const result = await txClient.signAndBroadcast([msg], { fee: { amount: fee,
                        gas: "200000" }, memo });
                return result;
            }
            catch (e) {
                if (e == MissingWalletError) {
                    throw new SpVuexError('TxClient:MsgAggregateValidatorVote:Init', 'Could not initialize signing client. Wallet is required.');
                }
                else {
                    throw new SpVuexError('TxClient:MsgAggregateValidatorVote:Send', 'Could not broadcast Tx: ' + e.message);
                }
            }
        },
        async sendMsgAggregateValidatorPrevote({ rootGetters }, { value, fee = [], memo = '' }) {
            try {
                const txClient = await initTxClient(rootGetters);
                const msg = await txClient.msgAggregateValidatorPrevote(value);
                const result = await txClient.signAndBroadcast([msg], { fee: { amount: fee,
                        gas: "200000" }, memo });
                return result;
            }
            catch (e) {
                if (e == MissingWalletError) {
                    throw new SpVuexError('TxClient:MsgAggregateValidatorPrevote:Init', 'Could not initialize signing client. Wallet is required.');
                }
                else {
                    throw new SpVuexError('TxClient:MsgAggregateValidatorPrevote:Send', 'Could not broadcast Tx: ' + e.message);
                }
            }
        },
        async sendMsgDelegateFeedConsent({ rootGetters }, { value, fee = [], memo = '' }) {
            try {
                const txClient = await initTxClient(rootGetters);
                const msg = await txClient.msgDelegateFeedConsent(value);
                const result = await txClient.signAndBroadcast([msg], { fee: { amount: fee,
                        gas: "200000" }, memo });
                return result;
            }
            catch (e) {
                if (e == MissingWalletError) {
                    throw new SpVuexError('TxClient:MsgDelegateFeedConsent:Init', 'Could not initialize signing client. Wallet is required.');
                }
                else {
                    throw new SpVuexError('TxClient:MsgDelegateFeedConsent:Send', 'Could not broadcast Tx: ' + e.message);
                }
            }
        },
        async MsgAggregateValidatorVote({ rootGetters }, { value }) {
            try {
                const txClient = await initTxClient(rootGetters);
                const msg = await txClient.msgAggregateValidatorVote(value);
                return msg;
            }
            catch (e) {
                if (e == MissingWalletError) {
                    throw new SpVuexError('TxClient:MsgAggregateValidatorVote:Init', 'Could not initialize signing client. Wallet is required.');
                }
                else {
                    throw new SpVuexError('TxClient:MsgAggregateValidatorVote:Create', 'Could not create message: ' + e.message);
                }
            }
        },
        async MsgAggregateValidatorPrevote({ rootGetters }, { value }) {
            try {
                const txClient = await initTxClient(rootGetters);
                const msg = await txClient.msgAggregateValidatorPrevote(value);
                return msg;
            }
            catch (e) {
                if (e == MissingWalletError) {
                    throw new SpVuexError('TxClient:MsgAggregateValidatorPrevote:Init', 'Could not initialize signing client. Wallet is required.');
                }
                else {
                    throw new SpVuexError('TxClient:MsgAggregateValidatorPrevote:Create', 'Could not create message: ' + e.message);
                }
            }
        },
        async MsgDelegateFeedConsent({ rootGetters }, { value }) {
            try {
                const txClient = await initTxClient(rootGetters);
                const msg = await txClient.msgDelegateFeedConsent(value);
                return msg;
            }
            catch (e) {
                if (e == MissingWalletError) {
                    throw new SpVuexError('TxClient:MsgDelegateFeedConsent:Init', 'Could not initialize signing client. Wallet is required.');
                }
                else {
                    throw new SpVuexError('TxClient:MsgDelegateFeedConsent:Create', 'Could not create message: ' + e.message);
                }
            }
        },
    }
};
